# OpenSCAD Teil 1: 3D Modelieren mit OpenSCAD

<img src="images/openSCAD_3DModelingWithOpenSCAD_FeatureImg.png" width="960"/><br>

## Beschreibung

In einer nachhaltigen Zukunft gilt es, kürzere Zyklen für die Wiederverwendung von Materialien wie Holz und Biokunststoff zu schaffen. Einer der möglichen Kreisläufe ist das direkte Recycling durch lokale oder einheimische Hersteller. Maschinen wie 3D-Drucker, CNC-Fräsmaschinen und Lasercutter erleben derzeit eine rasante technische Evolution, sodass in naher Zukunft immer mehr benutzerfreundliche Geräte verfügbar sein werden.

OpenSCAD ist ein skriptbasiertes Open-Source-CAD-Programm (Computer Aided Design), das mit seiner sehr einfachen Syntax jedem Interessierten ermöglicht, eigene Designs zu erstellen.

OpenSCAD-Kurs Teil 1 vermittelt anhand von Beispielen und Gruppenübungen die Grundlagen zum Modellieren eigener Designs in OpenSCAD. Die [syntax](syntax/OpenSCAD_Syntax_A4.pdf) der Modellierungssprache von OpenSCAD passt auf eine A4-Seite und ist sehr leicht verständlich.

Der Kurs erfordert keine Programmierkenntnisse. Die Syntax ist dem menschlichen Verständnis so nahe, dass nicht einmal ein Computer benötigt wird, um daraus Architekturzeichnungen zu erstellen.

---

## Index <a name="index"></a>

[Vorbereitung](#preparation)

---

## Index <a name="index"></a>

1. [Eine Einleitung](#introduction)

  1.1. [Warum Code zum Erstellen von 3D-Objekten verwenden?](#whycode)

  1.2. [Was ist OpenSCAD?](#whatsopenscad)

  1.3. [Wie funktioniert OpenSCAD?](#howdoesitwork)

2. [Erste Schritte in OpenSCAD](#firststeps)

  2.1. [Grafische Benutzeroberfläche](#gui)

  2.2. [Kommentiere Code](#comments)

  2.3. [Boolesche Operationen](#booleans)

  2.4. [Transformationen](#transformations)

  2.5. [3D Primitive](#3dprimitives)

  2.6. [2D Primitive](#2dprimitives)

  2.7. [Text](#text)

  2.8. [Extrusion](#extrusion)

  2.9. [Projektion](#projection)

  2.10. [Export](#export)

  2.11. [Import](#import)

  2.12. [Schleifen](#loops)

  2.13. [Statements](#statements)

  2.14. [Module](#modules)

  2.15. [Variablen](#variables)


3. [Beispiele](#examples)

  3.1. [3D Formen](#e3dshapes)

  3.2. [2D Formen](#e2dshapes)

  3.3. [Text](#etext)

  3.4. [Transformationen](#etransform)

  3.5. [Extrusion](#eextrude)

  3.6. [Module](#emodules)

  3.7. [Schleifen](#eloops)

  3.8. [Mathe](#emath)

  3.9. [Variablen](#evariables)

---

## Vorbereitung<a name="preparation"></a>

Es gibt 3 Möglichkeiten, mit OpenSCAD zu arbeiten:

1. Für den Kurs musst du OpenSCAD auf deinem PC / Laptop installieren. Es ist für Linux, Mac und Windows verfügbar.

  [Download](https://openscad.org/downloads.html) OpenSCAD für deine Plattform und installiere es.

2. Es gibt eine eingeschränkte Version für ein Android-Gerät namens [ScorchCAD](https://play.google.com/store/apps/details?id=com.scorchworks.scorchcad), falls du damit arbeiten möchtest.

3. Eine andere Möglichkeit ist eine Online-Version namens [OpenJSCAD](http://openjscad.azurewebsites.net/), die keine Installation benötigt.

---

#### [Zurück zum Index](#index)

## 1. Eine Einleitung <a name="introduction"></a>

---

#### [Zurück zum Index](#index)

## Index <a name="index"></a>

## 1.1. Warum Code zum Erstellen von 3D-Objekten verwenden? <a name="#whycode"></a>

* Gemeinsame Nutzung von Funktionen >> In allen modernen Entwicklungssprachen gibt es immer eine Möglichkeit, frühere Arbeiten und sogar die Arbeit anderer zu nutzen.
* Strukturelle Änderungen an bestehenden Teilen vornehmen >> Mit parametrischem Design mit Variablen können Sie vorhandene Designs einfach anpassen und modifizieren.
* Direkt von der Skizze zum Objekt >> Mit Code kannst du genau definieren, wie die Dinge in einer bestimmten Reihenfolge ablaufen sollen.
* Dinge wiederverwenden >> OpenSCAD hat einen sehr nützlichen Satz von Funktionen, die „Module“ genannt werden. Ein Modul ist einer Methode oder Funktion in alltäglichen C-basierten Sprachen sehr ähnlich. Bei einem Modul kann ich sagen „mach dieses Ding“, nimm eine Reihe grundlegender Änderungen vor und rufe dieses Modul so oft auf, wie ich möchte, und verbessere das Ergebnis.
* Programmiergrundlagen lernen >> Der praktische Umgang mit Objekten, die mit OpenSCAD-Skripten erstellt wurden, führt dich in die Grundlagen ein, die zum Erlernen einer Computersprache unerlässlich sind.
* Erhöht die Problemlösungsfähigkeiten >> Ein algorithmischer Ansatz bei der Erstellung eines Designs hilft im Allgemeinen bei der Problemlösung im täglichen Leben, da er dir hilft, konstruktives Denken zu lernen oder zu verbessern.

---

#### [Zurück zum Index](#index)

## Index <a name="index"></a>

## 1.2. Was ist OpenSCAD? <a name="whatsopenscad"></a>

* OpenSCAD ist eine 2D/3D-Modellierungssoftware, die auf einer funktionalen Programmiersprache basiert.
* OpenSCAD wird verwendet, um solide 3D-CAD-Objekte (Computer-Aided-Design) zu erstellen, die auf dem Bildschirm in der Vorschau angezeigt wird.
* Diese Objekte können dann gerendert werden, um sie in einer Vielzahl von 2D/3D-Dateiformaten zu exportieren.
* Es ist kostenlose Software und verfügbar für Linux/UNIX, MS Windows, Apples OS X, Android und als Online-Browseranwendung namens [OpenJSCAD] (https://openjscad.org).
* Im Gegensatz zu den meisten Programmen zum Erstellen von 3D-Modellen (wie Blender oder FreeCAD) erstellt OpenSCAD 3D-Modelle über einen für Menschen lesbaren Code, der es ermöglicht, einen Bauplan ohne Verwendung eines Computers zu erstellen.
* OpenSCAD ist so etwas wie ein 3D-Compiler, der eine Skriptdatei einliest, die das Objekt beschreibt und das 3D-Modell aus dieser Skriptdatei rendert.
* OpenSCAD ermöglicht es einem Designer, genaue 3D-Modelle und parametrische Designs zu erstellen, die durch Ändern von Parametern leicht angepasst werden können.
* OpenSCAD kann 3D-Modelle als STL, DXF, SVG und/oder als PNG exportieren.
* FreeCAD verfügt über einen hervorragenden Importer für .scad-Dateien, sodass Sie alle Teile zusammenbauen und technische Dokumentationen erstellen, sowie OpenSCAD-Designs in komplexen Projekten implementieren können (z. B. Haus- oder Maschinenentwürfe).

#### Cons vs Pro

**Cons**
* Es ist schwierig, in der 3D Vorschau ein Objekt zu navigieren.
* Es kann nicht mit Shadern für realistische Renderings genutzt werden.
* Es ist mühsam, damit Animationen zu machen.
* Auch der Zusammenbau mehrerer Teile ist nicht einfach.

**Pros**
* Software ist leicht.
* Code als Textformat hat eine viel kleinere Dateigröße, als Projektdateien anderer Programme.
* Es ist so einfach zu lesen, dass du keinen Computer benötigst, um daraus einen Bauplan zu erstellen.
* Es ist perfekt für Designer und Hersteller, die weniger komplexe Produkte erstellen.


---

#### [Zurück zum Index](#index)

## 1.3. Wie funktioniert OpenSCAD? <a name="howdoesitwork"></a>

OpenSCAD basiert auf CSG oder Construtive Solid Geometry. Es stellt ein Objekt dar, indem es kombiniert wird Grundformen in einer bestimmten Hierarchie von hauptsächlich booleschen Operationen.

Diese Operationen können wie folgt erklärt werden:

* **NICHT** kann als jeder Punkt ausgedrückt werden, der innerhalb von Polygon A, aber nicht innerhalb von B liegt, und umgekehrt, wodurch effektiv eine Subtraktion des einen vom anderen durchgeführt wird, bekannt als ***Differenz*** (difference).
* **ODER** kann als jeder Punkt ausgedrückt werden, der sich innerhalb von Polygon A oder B befindet, daher auch als ***Union*** (union) bekannt.
* **AND** kann als jeder Punkt ausgedrückt werden, der innerhalb der beiden Polygone A und B liegt und somit den ***Schnittpunkt*** (intersection) von A und B darstellt.
* **XOR** kann als jeder Punkt ausgedrückt werden, der nur in Polygon A oder B liegt, wobei alle Punkte weggelassen werden, die auf dem ***Schnittpunkt*** (intersection) von A und B liegen.

OpenSCAD verwendet folgende Syntaxprinzipien, die wir in diesem Kurs Stück für Stück lernen werden:

>Objekt();<br>
>Variable = Wert;<br>
>Operator() Aktion();<br>
>Operator () { Aktion (); Aktion(); }<br>
>Operator () Operator () { Aktion (); Aktion(); }<br>
>Operator () { Operator () Aktion (); Operator () { Aktion (); Aktion(); } }

**Objekte:**

* sind Bausteine für Modelle, erstellt aus 2D- und 3D-Primitiven.
* enden mit einem Semikolon (;).

**Aktionen:**

* umfassen Anweisungen, das Erstellen von Objekten mit Primitiven und das Zuweisen von Werten zu Variablen.
* enden mit einem Semikolon (;).

**Operatoren:**

* oder Transformationen, ändern die Position, Farbe und andere Eigenschaften von Objekten.
* können für dieselbe Aktion oder Aktionsgruppe in unterschiedlicher Art verwendet werden.
* die der Aktion am nächsten sind, werden zuerst verarbeitet.
* enden **nicht** mit einem Semikolon (;).


**Werte:**

* sind Zahlen (wie 42), ein Boolean (wie wahr), ein Text (wie "foo"), ein Bereich (wie [0:1:10]) oder der Undef-Wert (undef).
* kann in Variablen gespeichert, als Funktionsargument übergeben oder als Funktionsergebnis zurückgegeben werden.

***Fangen wir an! Schauen wir uns zuerst die grafische Benutzeroberfläche der Software an!***      


---

#### [Zurück zum Index](#index)

## 2. Erste Schritte in OpenSCAD <a name="firststeps"></a>

<img src="images/2_FirstSteps_1.png" width="960"/><br>

---

#### [Zurück zum Index](#index)

### 2.1. Grafische Benutzeroberfläche <a name="gui"></a>

Die Benutzeroberfläche der grafischen OpenSCAD-Anwendung hat 3 Hauptfenster: Texteditor, Konsolenfenster und den Anzeigebereich/Anzeigefenster.

#### Text Editor

<img src="images/2_1_FirstSteps_1.png" width="800"/><br>

Der Texteditor bietet grundlegende Bearbeitungsfunktionen wie Textsuche und -ersetzung (STRG + F), einschließlich Syntaxhervorhebung. Vordefinierte Farbschemata können über den Voreinstellungen-Dialog geändert werden.

<img src="images/2_1_FirstSteps_2.png" width="512"/><br>

Symbole des Texteditors (von links nach rechts):

1. Erstelle eine neue Datei (STRG + N)
2. Öffne eine vorhandene Datei (STRG + O)
3. Speichere die aktuelle Datei (STRG + S)
4. Letzten Schritt rückgängig machen (STRG + Z)
5. Mache das rückgängig gemachte rückgängig (STRG + UMSCHALT + Z)
6. Einrücken einer Zeile oder eines ausgewählten Blocks aufheben (STRG + UMSCHALT + I)
7. Eine Zeile oder einen ausgewählten Block einrücken (STRG + I)
8. Code im Vorschaumodus ausführen (F5)
9. Code im Rendermodus ausführen (F6)
10. Gerendertes Objekt im STL Format exportieren (F6 muss zuerst gedrückt werden)
11. Senden Sie ein gerendertes Objekt direkt an einen angeschlossenen Drucker oder an OctoPrint (eine 3D Druckerverwaltungsplattform)


#### Vorschaubereich

<img src="images/2_1_FirstSteps_3.png" width="800"/><br>

Die 3D Vorschaubereich zeigt eine Vorschau oder eine gerenderte Version des im Texteditor beschriebenen Designs.

Es ist möglich, die Perspektive zu ändern und sich um das Objekt zu bewegen.

Um das Objekt innerhalb des Raums zu verschieben, musst du jedoch **translate()** oder **rotate()** verwenden.

<img src="images/2_1_FirstSteps_4.png" width="512"/><br>

Vorschaubereichssymbole (von links nach rechts):

1. Code im Vorschaumodus ausführen (F5)
2. Code im Rendermodus ausführen (F6)
3. Alle vom Code erstellten Objekte anzeigen (STRG + UMSCHALT + V)
4. Hineinzoomen
5. Rauszoomen
6. Ansicht auf Offset zurücksetzen
7. Zur rechten Ansicht wechseln (STRG + 7)
8. Zur Draufsicht wechseln (STRG + 4)
9. Zur Ansicht von unten wechseln (STRG + 5)
10. Zur linken Ansicht wechseln (STRG + 6)
11. Zur Vorderansicht wechseln (STRG + 8)
12. Zur Rückansicht wechseln (STRG + 9)
13. Zur perspektivischen Ansicht wechseln
14. Zur orthografischen Ansicht wechseln
15. Achse ein-/ausschalten (STRG + 2)
16. Skalenmarkierungen in Millimetern ein-/ausschalten
17. Zeigt Drahtgitter von Polygonen, die das Objekt erzeugen (STRG + 1) >> !Funktioniert nur im Vorschaumodus F5!


#### Konsolenfenster

<img src="images/2_1_FirstSteps_5.png" width="512"/>

Die Konsolenansicht zeigt wichtige Statusinformationen, Warnungen und Fehler an.


#### Navigation im Vorschau-3D-Port

Um dir zu zeigen, wie du dich innerhalb des 3D-Ansichtsfensters um ein Objekt bewegst, erstellen wir unser erstes Objekt und zeigen es in der Vorschau an.

1. Geben Sie im Texteditor folgenden Code ein:

    sphere(30);

  **Semikolon am Ende nicht vergessen!**

2. Presse **F5**.

Folge nun den Anweisungen, um zu erfahren, wie du dich im 3D-Raum des Vorschauports bewegen.

* Bewege den Mauszeiger über den Anzeigebereich und drücke die LMB (linke Maustaste) und ziehe die Maus, um sich im dreidimensionalen Raum um das Objekt zu drehen.

* Drücke die MMB (mittlere Maustaste) und ziehe die Maus, um in die Richtung des Objekts zu zoomen. Du kannst auch das Scrollrad verwenden, falls deine Maus eines hat.

* Drücke nun die RMB (rechte Maustaste) und ziehe die Maus, um die Ansicht zweidimensional zu verschieben.

* Ändere die Fenstergröße, indem du den Cursor über die Ränder bewegst, bis sich das Symbol ändert, klicke auf die LMB und ziehe den Fensterrand.

* Die folgenden Zeichen, die vor einer codierten Zeile verwendet werden, helfen Dir beim Designprozess und eröffnen die Möglichkeit, einen Teil des Designs hervorzuheben oder herauszuheben.

  | Zeichen | Vorschauoption    |
  |-----------|--------------------|
  | \*        | Deaktiviere das Objekt |
  | \!    | Zeigt nur dieses Objekt an |
  | \#      | Markiere das Objekt |
  | \% | Machen Sie das Objekt transparent |

#### Übung:

1. Schreiben Sie 3-Zeilen-Code mit ***cylinder(40);***, ***sphere(30);*** und ***cube(35);*** in jede Zeile.
2. Presse **F5**.
3. Füge dann verschiedene Änderungszeichen vor einer Zeile hinzu.
4. Presse wieder **F5**.

Beispiel:

    sphere(30);
    cylinder(40);
    %sphere(30);
    #cube(35);


---

#### [Zurück zum Index](#index)

### 2.2. Kommentiere Code <a name="comments"></a>

Das Kommentieren des Codes ist, als würdest du Skriptnotizen für dich selbst hinterlassen und/oder Anderen helfen, sie zu verstehen.

Kommentare werden vom Kompilierer, der die Vorschau oder das 3D-Netz erstellt, ignoriert.

Es ist auch nützlich, eine Kopfzeile als mehrzeiligen Kommentar zu erstellen, um Dateinamen, Beschreibung, Autor usw. hinzuzufügen.

Mehrzeilige Kommentare oder das Auskommentieren von Codeblöcken verwenden Schrägstrich und Asterix zum Kommentieren, während Asterix und Schrägstrich zum Auskommentieren verwendet werden.

    /*
    Mehrzeilige Kommentare
    */

Einzelne Zeilen können mit zwei Schrägstrichen kommentiert werden.

    //Einzeiliger Kommentar

Einzeilige Kommentare können automatisiert werden, indem du die Zeile oder mehrere Zeilen auswählen und STRG + D drückst. Drücke UMSCHALT + STRG + D in einer kommentierten Zeile oder einem kommentierten Abschnitt, um die Kommentierung aufzuheben. Dies funktioniert nicht, wenn zwischen den Zeilen eine Leerzeile ist.

Bei einer Blockauswahl markiere alle Zeilen auf einmal. Drücke UMSCHALT + STRG + D, um die Kommentierung aufzuheben. Drücke STRG + D, um es erneut zu kommentieren.

    //Kommentarzeile 1
    //Kommentarzeile 2
    //Kommentarzeile 3

---

#### [Zurück zum Index](#index)

### 2.3. Boolesche Operationen <a name="booleans"></a>

Es gibt 3 boolesche Operationen in OpenSCAD: difference(), intersection() und union().

**Difference (Unterschied):**
* erstellt eine Form, die den Unterschied zwischen 2 Objekten oder einer Objektgruppe ausschneidet.
* es subtrahiert das 2. Objekt vom 1. Objekt.

**Intersection (Überschneidung):**
* erstellt eine Form aus einer Überlappung von 2 Objekten oder einer Objektgruppe.

**Union:**
* erstellt eine Form aus der äußeren Form von 2 oder mehr Objekten (Zoomen innerhalb des Objekts zeigt den Unterschied).

Die chronologische Reihenfolge der Objekte innerhalb des Operatorblocks ist von Bedeutung.

***Probiere den Code unten aus und vertausche die Positionen der Objekte!***

Difference:

    difference()
    {
      sphere(30);
      cube(35);
    }

<img src="images/2_3_FirstSteps_1.png" width="512"/><br>


Intersection:

    intersection()
    {
      sphere(30);
      cube(35);
    }

<img src="images/2_3_FirstSteps_2.png" width="512"/><br>

Union:

    union()
    {
      sphere(30);
      cube(35);
    }

<img src="images/2_3_FirstSteps_3.png" width="512"/><br>


---

#### [Zurück zum Index](#index)

### 2.4. Transformationen <a name="transformations"></a>

OpenSCAD hat verschiedene Funktionen für verschiedene Arten von Transformationen.
Raumorientierte Transformationen sind **rotate()** für die Rotation und **translate()** für die Translation. Formverändernde Transformationen sind **scale()** zum Skalieren, **mirror()** zum Erstellen gespiegelter Formen. Die anzeigeverändernde Transformation von Formen ist **color()**. Formbildende Transformationen sind **minkowski()**, **hull()** und **offset()**.

**translate()**

* Bewegt das Objekt oder eine Gruppe von Objekten entlang eines bestimmten Vektors.
* Argumente sind optional.
* Syntaxverwendung:

  >translate(v=[x, y, z]) {...}

Verschiebe die Objekte:

  cube(20);
  translate([20, 0, 0])
  cube(10);
  translate([30, 0, 0])
  cube(5);

<img src="images/2_4_FirstSteps_1.png" width="512"/><br>

**rotate()**

* Dreht das Objekt oder eine Gruppe von Objekten um eine oder mehrere Achsen des Koordinatensystems oder eine beliebige Achse.
* Argumentnamen sind optional, wenn sie in der angegebenen Reihenfolge vorliegen.
* Syntaxverwendung:

  >rotate(a=deg_a, v=[x, y, z]) {...}

* Argument a kann ein Vektor sein, sodass Argument v ignoriert wird.
* Argument v legt eine beliebige Achse fest, um die das Objekt gedreht wird.

Eine Box drehen:

    rotate(a=[0, 180, 0]) cube([20, 50, 30]);

<img src="images/2_4_FirstSteps_2.png" width="512"/><br>

ist das gleiche wie unten, während translate das nächste Feld daneben setzt:

    translate([ 40, 0, 0])
    rotate(a=180, v=[0, 1, 0]) cube([20, 50, 30]);

<img src="images/2_4_FirstSteps_3.png" width="512"/><br>

Mit einem einzelnen skalaren Argument drehen dreht sich um die Z-Achse, was für 2D-Objekte nützlich ist:

    translate([-50, 0, 0])
    rotate(45) square(30);

<img src="images/2_4_FirstSteps_4.png" width="512"/><br>

Die reguläre Verwendung ist rotieren([x, y, z]):

    translate([-20, -40, 0])
    rotate([45, 45, 0]) cube(20);

<img src="images/2_4_FirstSteps_5.png" width="512"/><br>


**scale()**

* Ändert die Größe eines Objekts mithilfe eines angegebenen Vektors.
* Der Argumentname ist optional.
* Syntaxverwendung:

  >scale(v=[x, y, z]) {...}

Skaliere ein Objekt:

    cube(30); //Default cube
    translate([-80, 0, 0]) //Move it aside
    scale(2)
    cube(30);
    translate([50, 0, 0]) //Move it aside
    scale([3, 2, 1]) //Scale
    cube(30);

  <img src="images/2_4_FirstSteps_6.png" width="512"/><br>


**mirror()**

* Spiegelt ein Objekt oder eine Objektgruppe auf einer Ebene durch den Ursprung.
* Das Argument ist ein Vektor, der die Spiegelung mit 1 aktiviert, während 0 keine Aktion anzeigt.
* Syntaxverwendung:

  >mirror([x, y, z]) {...}

Spiegele ein Objekt:

    rotate([0, 0, 10]) cube([30, 20, 10]);

Versuche verschiedene Spiegelachsen X, Y, und Z:

    mirror([1, 0, 0])
    translate([10, 0, 0])
    rotate([0, 0, 10])
    cube([30, 20, 10]);

  <img src="images/2_4_FirstSteps_7.png" width="512"/><br>


***Es gibt noch mehr Transformationen, die ich später erklären werde. Machen wir erstmal mit dem nächsten Kapitel weiter!***

**multmatrix()**

* Multipliziert die Geometrie eines Objekts oder einer Gruppe von Objekten mit einer gegebenen affinen Transformationsmatrix.
* Eine Matrix ist 4x3 - ein Vektor aus 3 Zeilenvektoren mit 4 Elementen.
* Eine andere Matrix ist 4x4 - ein Vektor aus 4 Zeilenvektoren, während die vierte Zeile immer [0,0,0,1] ist.
* Syntaxverwendung:

  >multmatrix(m=[...]) {...}

* Als nächstes eine Aufschlüsselung, was du mit unabhängigen Elementen in der Matrix machen kannst:

|   |   |   |    |
|---|---|---|----|
| [Skalieren X] | [Verschieben X along Y] | [Verschieben X along Z] | [Translation X] |
| [Verschieben Y along X] | [Skalieren Y] | [Verschieben Y along Z] | [Translation Y] |
| [Verschieben Z along X] | [Verschieben Z along Y] | [Skalieren Z] | [Translation Z] |

* Die vierte Zeile wird auf [0,0,0,1] gezwungen und wird daher nicht von OpenSCAD verarbeitet.

Verwenden Sie eine 3x4-Multimatrix auf einem Objekt, um eine Träne zu erstellen:

    angle=45;
    multmatrix(m=[[cos(angle), -sin(angle), 0, 10],
                [sin(angle), cos(angle), 0, -20],
                [0, 0, 1, 0],
                [0, 0, 0, 1]])
    union()
    {
        cylinder(r=10, h=10, center=false);
        cube(size=[10, 10, 10],center=false);
    }

<img src="images/2_4_FirstSteps_8.png" width="512"/><br>

Erstelle zuerst eine 4x4-Matrixvariable und wende sie dann auf Multmatrix an:

    M=[ [1, 0, 0, 0],
        [0, 1, 0.7, 30],
        [0, 0, 1, 0],
        [0, 0, 0, 1]];

Wende nun den Parameter auf eine Matrix an:

    multmatrix(M) {
        union()
        {
            cylinder(r=10, h=10, center=false);
            cube(size=[10, 10, 10],center=false);
        }
    }

<img src="images/2_4_FirstSteps_9.png" width="512"/><br>


**color()**

* Zeigt das Objekt in einer bestimmten RGB-Farbe + Alpha-Wert an.
* Es ist nur im Vorschaumodus **F5** sinnvoll, da die Farbe nicht auf das gerenderte Objekt übertragen wird.
* Der Alpha-Standardwert ist 1,0 (undurchsichtig).
* Syntaxverwendung:

  >color([r, g, b, a]) {...}

* r ist ROTER KANAL, g ist GRÜNER KANAL, b ist BLAUER KANAL.
* Jeder Kanal enthält einen Wert zwischen 0 und 1/[1:255].
* Farben können auch nach Namen definiert werden.

Hier alle Farbnamen:

| Color category | Color names | Color category | Color names | Color category | Color names |
|----------------|-------------|----------------|-------------|----------------|-------------|
| Purples  |  Lavender<br> Thistle<br> Plum<br> Violet<br> Orchid<br> Fuchsia<br> Magenta<br> MediumOrchid<br> MediumPurple<br> BlueViolet<br> DarkViolet<br> DarkOrchid<br> DarkMagenta<br> Purple<br> Indigo<br> DarkSlateBlue<br> SlateBlue<br> MediumSlateBlue | Pinks  | Pink<br> LightPink<br> HotPink<br> DeepPink<br> MediumVioletRed<br> PaleVioletRed  | Blues  | Aqua<br> Cyan<br> LightCyan<br> PaleTurquoise<br> Aquamarine<br> Turquoise<br> MediumTurquoise<br> DarkTurquoise<br> CadetBlue<br> SteelBlue<br> LightSteelBlue<br> PowderBlue<br> LightBlue<br> SkyBlue<br> LightSkyBlue<br> DeepSkyBlue<br> DodgerBlue<br> CornflowerBlue<br> RoyalBlue<br> Blue<br> MediumBlue, DarkBlue<br> Navy<br> MidnightBlue  |
| Reds  | IndianRed<br> LightCoral<br> Salmon<br> DarkSalmon<br> LightSalmon<br> Red<br> Crimson<br> FireBrick<br> DarkRed  | Greens  |  GreenYellow<br> Chartreuse<br> LawnGreen<br> Lime<br> LimeGreen<br> PaleGreen<br> LightGreen<br> MediumSpringGreen<br> SpringGreen<br> MediumSeaGreen<br> SeaGreen<br> ForestGreen<br> Green<br> DarkGreen<br> YellowGreen<br> OliveDrab<br> Olive<br> DarkOliveGreen<br> MediumAquamarine<br> DarkSeaGreen<br> LightSeaGreen<br> DarkCyan<br> Teal | Oranges | LightSalmon<br> Coral<br> Tomato<br> OrangeRed<br> DarkOrange<br> Orange  |
| Yellows  |  Gold<br> Yellow<br> LightYellow<br> LemonChiffon, LightGoldenrod<br> Yellow<br> PapayaWhip<br> Moccasin<br> PeachPuff<br> PaleGoldenrod<br> Khaki<br> DarkKhaki | Browns  | Cornsilk<br> BlanchedAlmond<br> Bisque<br> NavajoWhite<br> Wheat<br> BurlyWood<br> Tan<br> RosyBrown<br> SandyBrown<br> Goldenrod<br> DarkGoldenrod<br> Peru<br> Chocolate<br> SaddleBrown<br> Sienna<br> Brown<br> Maroon   | Whites  | White<br> Snow<br> Honeydew<br> MintCream<br> Azure<br> AliceBlue<br> GhostWhite<br> WhiteSmoke<br> Seashell<br> Beige<br> OldLace<br> FloralWhite<br> Ivory<br> AntiqueWhite<br> Linen<br> LavenderBlush<br> MistyRose  |
| Grays  | Gainsboro<br> LightGrey<br> Silver<br> DarkGray<br> Gray<br> DimGray<br> LightSlateGray<br> SlateGray<br> DarkSlateGray<br> Black |   |   |   |   |

Ein dritter Weg ist die Verwendung von Hex-Werten, die in 4 Formanten angegeben werden können, #rgb #rgba #rrggbb #rrggbbaa.

Give objects color:

    color("red") translate([-40, 0, 0]) cube(20);
    color([0,1,0]) translate([-10, 0, 0]) cube(20);
    color("#0000ff") translate([20, 0, 0]) cube(20);

<img src="images/2_4_FirstSteps_10.png" width="512"/><br>

Try to add some alpha:

    color([1, 1, 1, 0.3]) translate([-50, -10, 0]) cube([100, 40, 40]);

<img src="images/2_4_FirstSteps_11.png" width="512"/><br>


**offset()**

* Erstellt eine neue 2D-Innen- oder Außenkontur.
* Es gibt 2 Betriebsarten, radial und versetzt.
* Offset erzeugt einen Umriss mit fester Umgebung, während sich radial wie ein umgebender Kreis verhält.
* Konturen können optional mit einer Fase versehen werden.
* Offset ist nützlich, um dünne Wände zu erstellen
* Parameter sind:

  **r**

  *Betrag zum Versetzen des Polygons. Wenn negativ, wird das Polygon nach innen gedreht.*

  *Es gibt den Radius des Kreises im Radiusmodus an.*

  **delta**

  *Abstand zwischen Original- und Offset-Polygonlinie. Negativ platziert es nach innen.*

  *Es wird für den Offset-Modus verwendet.*

  **chamfer**

  *Boolean (Standard: false). Gibt an, ob Kanten abgeschnitten (gefast) sind.*

Build a little vase:

    linear_extrude(height=60, twist=90, slices=60)
    {
        difference()
        {
            offset(r=10)
            {
                square(20, center=true);
            }
            offset(r=8)
            {
                square(20, center=true);
            }
        }
    }

<img src="images/2_4_FirstSteps_12.png" width="512"/><br>


**minkowski()**

* Zeigt die Minkowski-Summe der untergeordneten Knoten an.
* Es ist sinnvoll, runde Ecken für Kästen oder flache Körper (Kugeln oder Zylinder) zu erstellen.
* Minkowski ist sehr rechenintensiv, sodass Ihre CPU sehr damit beschäftigt sein kann.
* Syntaxverwendung:

  >minkowski() {...}

Minkowski example:

    minkowski()
    {
        sphere(10);
        cube([80, 60, 30]);
    }

<img src="images/2_4_FirstSteps_13.png" width="512"/><br>


**hull()**

* Zeigt die konvexe Hülle von Objekten an.
* Es kann neue Formen erstellen, indem es eine Gruppe von Objekten erstellt, die über einen Rumpf mit einem Netz verbunden werden können.
* Syntaxverwendung:

  >hull() {...}

  Table leg with hull():

      $fn=20;
      hull()
      {
          cylinder(h=0.01, d=3, $fn=$fn);
          translate([0, 20, 80])
          cylinder(h=0.01, d=10, $fn=$fn);
      }

  <img src="images/2_4_FirstSteps_14.png" width="512"/><br>

#### Übung:
***Versuche einen Boden für die Vase zu schaffen, entweder mit hull() oder minkowski()!***

**Übung für hull:**

    hull()
    {
        translate([11, 11, 0])
        cylinder(h=2, d=16);
        translate([-11, 11, 0])
        cylinder(h=2, d=16);
        translate([11, -11, 0])
        cylinder(h=2, d=16);
        translate([-11, -11, 0])
        cylinder(h=2, d=16);
    }

<img src="images/2_4_FirstSteps_15.png" width="512"/><br>

**Übung für minkowski:**

    minkowski()
    {
        translate([0, 0, 1])
        cube([22, 22, 2], center=true);
        cylinder(h=2, d=16);
    }

<img src="images/2_4_FirstSteps_16.png" width="512"/><br>

---

#### [Zurück zum Index](#index)

### 2.5. 3D Primitive <a name="3dprimitives"></a>

Es gibt drei grundlegende 3D-Primitive in OpenSCAD sind **cube()**, **sphere()** und **zylinder()**.

**cube();**

* Erstellt einen Würfel am Ursprung des Koordinatensystems, platziert den Würfel jedoch in seinem ersten Oktanten.
* Die Vorlagensyntax für cube:

  >cube(size=x,center=true/false); or cube(size=[x,y,z],center=true/false);

* Mit center=true wird der Ursprung des Würfels mit dem Ursprung des Koordinatensystems abgeglichen.
* Um unterschiedliche Größen für jede Ebene entlang einer Achse anzuwenden, werden eckige Klammern verwendet -> [x,y,z].
* Äquivalenter Code:

    cube(size=8);<br>
    cube(18);<br>
    cube([8, 18, 28]);<br>
    size=38;<br>cube(size);<br>
    size=[38, 48, 58];<br> cube(size);

    translate([-60, 0, 0])
    cube(30);               //cube with squared sizes all around
    cube(30, center=true);   //squared cube with center in offset 0,0,0 of the coordinate system
    translate([30, 0, 0])
    cube([30, 50, 80]);   //cube with different sizes in x, y, and z


<img src="images/2_5_FirstSteps_2.png" width="512"/><br>


*Probieren Sie diese Codeversion mit translate() aus, um alle Versionen nebeneinander zu positionieren!*

    translate([200, 0, 0])
    cube(size=8);

    translate([160, 0, 0])
    cube(18);

    translate([120, 0, 0])
    cube([8, 18, 28]);

    size1=38;
    translate([60, 0, 0])
    cube(size1);

    size2=[38, 48, 58];
    translate([0, 0, 0])
    cube(size2);

<img src="images/2_5_FirstSteps_1.png" width="512"/><br>

**sphere();**

* Erstellt eine Kugel am Ursprung des Koordinatensystems.
* Parameter sind:

  **r**

  *Radius, während r nicht zugewiesen werden muss, da die Dezimalstelle automatisch an Radius adressiert wird.*

  **d**

  *Durchmesser der Kugel.*

  **$fa**

  *Fragmentwinkel in Grad.*

  **$fs**

  *Fragmentgröße in mm.*

  **$fn**

  *Auflösung*

* Standardwerte der **sphere()**-Parameter ergeben:

  >sphere(r=10, $fn=0, $fa=12, $fs=1);


    radius=5;  //parametric value describing radius, change  it to 1, 5, 10
    $fn=20; //parametric value describing resolution, change it to 4,10, 40, 80
    translate([0, 0, radius]) //move it up by radius
    sphere(radius, $fn=$fn); //create sphere with assigned radius

<img src="images/2_5_FirstSteps_4.png" width="512"/><br>

#### Übung:

*Erstelle 2 Variablen namens Radius und $fn und wende sie auf eine Kugel an. Sehe dir dann die Änderungen an!*

    translate([60, 0, 0])
    sphere(10);

    translate([30, 0, 0])
    sphere(r=20, $fn=3);

    translate([0, 0, 0])
    sphere(r=10, $fa=50);

    translate([-30, 0, 0])
    sphere(r=8, $fs=3);

    translate([-65, 0, 0])
    sphere(r=15, $fn=200);

<img src="images/2_5_FirstSteps_3.png" width="512"/><br>



**cylinder();**

* Erstellt einen Zylinder am Ursprung des Koordinatensystems, während der Drehpunkt des Zylinders unten ist.
* Mit center=true wird der Zylinder in der Mitte der Form versetzt, auch auf der Z-Achse.
* Die Benennung der Parameter ist optional, muss jedoch in der richtigen Reihenfolge verwendet werden.
* Daher ist es sinnvoll, die Parameternamen zu verwenden, um die Zylinderform zu definieren.
* Parameter:

  **h**

  *Höhe des Zylinders oder Kegels*

  **r**

  *Zylinderradius - r1=r2=r*

  **r1**

  *Bodenradius des Kegels*

  **r2**

  *Spitzenradius des Kegels*

  **d**

  *Zylinderdurchmesser - r1=r2=d/2*

  **d1**

  *Durchmesser Kegelboden - r1=d2/2*

  **d2**

  *Durchmesser Kegelspitze - r2=d2/2*

  **center**

  *false (default), true*

  **$fa**

  *minimaler Winkel (in Grad) jedes Fragments*

  **$fs**

  *minimale Umfangslänge jedes Fragments*

  **$fn**

  *feste Anzahl von Fragmenten in 360 Grad*

* Wenn du in dieser Liste einen Parameternamen verwendest: h, r, r1, r2, d1, d2, musst du alle weiteren verwendeten Parameter benennen.
* $fa, $fs, $fn müssen immer benannt werden (universal, nicht nur bei Zylinder).
* Die Verwendung von Zylindern zum Herstellen von Löchern in einer Form muss eine hohe $fn-Auflösung haben und etwas größer als die gewünschte Größe sein.


    cylinder(d=20,h=8);

<img src="images/2_5_FirstSteps_5.png" width="512"/><br>

Schaufenster der umschriebenen Löcher:

    color([1, 1, 0])cylinder(h=20, d=20, $fn=80);
    color([1, 0, 0]) cylinder(h=25, d=20, $fn=8);
    color([0, 1, 0]) cylinder(h=15, d=21.7, $fn=8); //This is a circumscribed hole

<img src="images/2_5_FirstSteps_6.png" width="512"/><br>

#### Übung:

*Erstelle einen Zylinder, eine Pyramide und einen Kegel!*

    cylinder(20, 10, center=true);  //Not using the parameter names: First number = d, second number = r1, rest remains default (r2=1)
    translate([-30, 0, 0])
    cylinder(20, r1=10, r2=0); //Using the parameter name r will dedicate the same number to top and bottom
    translate([30, 0, 0])
    rotate([0, 0, 45])
    cylinder(h=20, r1=10, r2=0, $fn=4); //A cylinder using all parameters
    translate([60, 0, 0])
    cylinder(h=20, d=20); //A cylinder using all parameters

<img src="images/2_5_FirstSteps_7.png" width="512"/><br>

**polyhedron();**

* is an advanced and therefore most general primitive solid
* it can create any regular or irregular shape including shapes with concave and convex features
* this is only mentioned here to complete the list of 3D solids. We will not use this shape at all.
* There are many ways to do shapes by combining basic shapes (see the code below)
If it is not possible to create a shape in other ways, polyhedron() can achieve it
Polyhedrons contain points and faces ->

  >polyhedron(points[[x,y,z],...],faces=[[p1,p2,p3],...],convexity=N);

Du kannst in deinem Design verschiedene Methoden verwenden, um die gleichen Ergebnisse zu erzielen. Hier ein Beispiel:

    polyhedron(points=[[0, -10, 60], [0, 10, 60], [0, 10, 0], [0, -10 ,0], [60, -10, 60], [60, 10, 60]],
    faces=[[0, 3, 2], [0, 2, 1], [3, 0, 4], [1, 2, 5], [0, 5, 4], [0, 1, 5], [5, 2, 4], [4, 2, 3]]);
    //Or 2 cubes and intersection
    translate([0, -60, 0])
    difference()
    {
        rotate([0, 0, -90])
        cube([20, 60, 60]);
        rotate([-45, 0, -90])
        cube([20, 60, 100]);
    }

<img src="images/2_5_FirstSteps_8.png" width="512"/><br>

#### Übung:

**Erstelle ein Möbelstück wie einen Tisch, Stuhl, eine Lampe oder ähnliches!**

*Verwenden Sie Transformationen und 3D-Formen, die Sie gerade gelernt haben!*


---

#### [Zurück zum Index](#index)

### 2.6. 2D Primitive <a name="2dprimitives"></a>

2D-Primitive können verwendet werden, um durch Extrusion in 3D-Objekte umgewandelt zu werden (Beispiel Offset).

Obwohl sie unendlich dünn sind, werden sie mit 1 mm Dicke in der Vorschau angezeigt und gerendert.

Es gibt 4 verschiedene 2D-Primitive in OpenSCAD: **square()**, **circle()** und **polygon()**.

**square();**

* Erstellt ein Quadrat oder Rechteck im ersten Quadranten.
* Bei Parameter center=true wird das Quadrat auf den Ursprung gelegt.
* Syntaxverwendung:
  >square(size,center); or square([size x, size y],center); or both without center

* Parameter sind:

  **size**:

  *Einzelwert für ein Quadrat, zwei Werte in einem Vektor für ein Rechteck*

  **center**:

  *Boolean (Standard: false), zentriert das Objekt in [0,0] des Koordinatensystems, wenn true*


    square(30);
    translate([-30, 0, 0])
    square([30, 60], center=true);

<img src="images/2_6_FirstSteps_1.png" width="512"/><br>


**circle()**

* Erstellt einen Kreis um den Ursprung.
* Alle Parameter außer r müssen benannt werden.
* Syntaxverwendung:
  >circle(r=radius|d=diameter);

* Parameter sind:

  **r**

  *Kreisradius, keine Benennung notwendig*

  **d**

  *Kreisdurchmesser*

  **$fa**

  *Minimaler Winkel jedes Fragments*

  **$fs**

  *Mindestumfangslänge jedes Fragments*

  **$fn**

  *Anzahl der Fragmente in 360 Grad*


    circle(15);
    translate([-40, 0, 0])
    circle(d=30, $fn=80);
    translate([40, 0, 0])
    circle(d=30, $fn=3); //Experiment with $fn to create more regular polygons

<img src="images/2_6_FirstSteps_2.png" width="512"/><br>

Eine Ellipse kann mit ***scale() circle();*** erstellt werden:

    scale([2, 0.5])
    circle(20);

<img src="images/2_6_FirstSteps_3.png" width="512"/><br>


**polygon()**

* Erstellt eine mehrseitige Form aus einer Liste von XY-Koordinaten.
* Dies ist das leistungsstärkste 2D-Objekt, einschließlich Formen mit konvexen und konkaven Kanten.
* Es ist auch möglich, Löcher innerhalb der Form zu platzieren.
* Syntaxverwendung:
  >polygon(points=[[x,y],...],paths=[[p1,p2,p3..],...], convexity = N);

* Parameter sind:

  **points**

  *Eine Liste von XY-Punkten des Polygons (Punkte werden von 0 bis n-1 indiziert).*

  **paths**

  *Alle Punkte werden in der angegebenen Reihenfolge verwendet.*

  **single vector**

  *Befehl zum Durchqueren der Punkte. Kann in anderer Reihenfolge mit ganz oder teilweise.*

  *Mehrere Vektoren: Erzeugt primäre und sekundäre Formen (wie Unterschied).*

  *Sekundärformen können ganz oder teilweise innerhalb der Primärform liegen.*

  **convexity**

  *Ganzzahlige Anzahl "einwärts gerichteter" Kurven, d. h. erwarteter Pfad kreuzt das Polygon.*

    polygon(points=[[0, 0], [100, 0], [130, 50], [30, 50]]);
    translate([-60, 0, 0])
    polygon([[-30, 0], [0, 60], [30, 0]]);
    translate([-40, 80, 0])
    polygon(points=[[0, 0], [100, 0], [0, 100], [10, 10], [80, 10], [10, 80]], paths=[[0, 1, 2], [3,4,5]], convexity=10);

<img src="images/2_6_FirstSteps_4.png" width="512"/><br>


---

#### [Back to Index](#index)

### 2.7. Text <a name="text"></a>

* Erstellt Text als geometrisches 2D-Objekt.
* Es verwendet Schriftarten, die auf dem lokalen System installiert sind oder eine als separate Schriftartdatei bereitgestellte Datei.
* Syntaxverwendung:
  >text(string);

* Parameter sind:

  **text**

  *String >> Der zu generierende Text*

  **size**

  *Dezimal (Standard: 10) >> Bestimmt die Höhe über der Schriftgröße*

  **font**

  *String >> Es ist nicht der Name der Schriftdatei, sondern der logische Name (Hilfe -> Schriftliste)*

  **halign**

  *String (Standard: links) >> Horizontale Ausrichtung für den Text "links" "Mitte" "rechts"*

  **valign**

  *String (Standard: baseline) >> Vertikale Ausrichtung für Text "top" "center" "baseline" "bottom"*

  **spacing**

  *Dezimal (Standard: 1) >> Zeichenabstand verringern/erhöhen, je größer, desto weiter auseinander*

  **direction**

  *String (Standard: "ltr") >> Textflussrichtung "ltr" - von links nach rechts, "rtl", "ttb", "btt"*

  **language**

  *String (Default: en) >> Sprache des Textes*

  **script**

  *String (Standard: "lateinisch") >> Skript der Sprache*

  **$fn**

  *Unterteilung von Kurvenbahnsegmenten durch Freetype*

Verwende ***linear_extrude(size_z) text(string)***, um den 3D-Text zu erstellen.

Text: Experimentieren Sie mit Textparametern

    text("Hello world!");
    translate([0, -10, 0])
    text("I am ready to model with code", size=5,font="Liberation Sans:style=Bold Italic");  
    translate([0, -30, 0])
    linear_extrude(5)
    text("A 3D Text", size=12, font="Liberation Sans");

<img src="images/2_7_FirstSteps_1.png" width="512"/><br>

---

#### [Zurück zum Index](#index)

### 2.8. Extrusionen <a name="extrusion"></a>

Ein Prozess zum Erstellen eines Objekts mit festem Querschnittsprofil.
Es gibt 2 Befehle, um 3D-Formen aus einer 2D-Form zu erstellen:

**linear_extrude()**

* Diese Operation verwendet ein 2D-Objekt als Eingabe und generiert als Ergebnis ein 3D-Objekt.
* Die Form von x und y wird entlang der z-Achse nach oben projiziert.
* Wenn du Transformationen verwendest, um das Objekt zu ändern, wird seine transformierte Version extrudiert.
* Syntaxverwendung:
  >linear_extrude(height,center,convexity,twist,slices,scale,$fn) {...}

* Parameter sind:

  **height**

  *z-Wert für resultierendes 3D-Objekt >> Muss positiv sein*

  **twist**

  *Grad, um den sich die Form während der Extrusion verdreht.*

  *Positiver Wert dreht sich im Uhrzeigersinn, negativer Wert dreht sich gegen den Uhrzeigersinn*

  **center**

  *Wenn center wahr ist, wird das extrudierte Objekt in der z-Ursprungsachse zentriert*

  **slices**

  *Anzahl der Zwischenpunkte entlang der Z-Achse der Extrusion. Seine Vorgabe erhöht sich*

  *mit Erhöhung des Drallwertes*

  **scale**

  *Skaliert die 2D-Form auf diesen Wert über die Höhe der Extrusion (Skalar oder Vektor)*

  **$fn**

  *Erhöht die Auflösung, was zu einer höheren Glätte führt*

Lineare Extrusion 1:

    linear_extrude(80, twist=360, slices=80)
    {
        rotate(45)
        scale([1.5, 0.5])
        square(10);
    }

<img src="images/2_8_FirstSteps_1.png" width="512"/><br>

Lineare Extrusion 2:

    linear_extrude(height=10, center=true, convexity=10, scale=3)
    translate([2, 0, 0])
    circle(r=1);

<img src="images/2_8_FirstSteps_2.png" width="512"/><br>


**rotate_extrude()**

* Like using a potter wheel, rotational extrusion spins a 2D shape around the Z-axis.
* It has rotational symmetry.
* The 2D shape must lie entirely on the left or right side of the Y-axis in order to work.
* Syntax Usage:
  >rotate_extrude(angle,convexity) {...}

* Parameters are:

  **angle**

  *Integer (Default: 360) >> Specifies number of degrees to sweep, starting at positive x*

  *Negative angle sweeps clockwise*

  **convexity**

  *Wenn die Extrusion bei einer nicht trivialen 2D-Form fehlschlägt, versuchen Sie es mit einem Wert wie 10 (empfohlen).*

  **$fa**

  *Mindestwinkel jedes Fragments*

  **$fs**

  *Mindestumfangslänge jedes Fragments*

  **$fn**

  *Feste Anzahl von Fragmenten in 360 Grad*


Rotationsextrusion 1 - Erstellen Sie einen Donut:

    rotate([0, 90, 0])
    rotate_extrude(convexity=10)
    translate([22, 0, 0]) //Change the y number
    circle(r=10);

<img src="images/2_8_FirstSteps_3.png" width="512"/><br>


Rotationsextrusion 2 - Erstellen Sie einen Haken:

    translate([0, 0, 100])
    rotate([-90, 0, 0])
    union()
    {
        translate([0, 60, 0])
        rotate_extrude(angle=270, convexity=10)
        translate([40, 0])
        circle(10);
        rotate_extrude(angle=90, convexity=10)
        translate([20, 0]) circle(10);
        translate([20, 0, 0])
        rotate([90, 0, 0]) cylinder(h=80, r=10);
    }

<img src="images/2_8_FirstSteps_4.png" width="512"/><br>

Extrudieren eines Polygons:

    rotate_extrude($fn=200) polygon(points=[[0, 0], [15, 0], [20, 5], [20, 20], [10, 25], [12, 50], [0, 50]]);

<img src="images/2_8_FirstSteps_5.png" width="512"/><br>

Ändern Sie den Code, um daraus ein echtes Vasendesign zu machen:

    rotate_extrude($fn=200) polygon(points=[[0, 0], [15, 0], [20, 5], [20, 20], [10, 25], [12, 50], [10, 50], [5, 5]]);

<img src="images/2_8_FirstSteps_6.png" width="512"/><br>


---

#### [Zurück zum Index](#index)

### 2.9. Projektionen <a name="projection"></a>

* Erstellt eine 2D-Zeichnung aus einem 3D-Modell
* Es projiziert ein 3D-Objekt auf die XY-Ebene mit z=0
* Die Parameter:

  **cut**

  *Bei cut=true werden nur die Punkte mit z=0 für die 2D-Form berücksichtigt*

  *Bei cut=false werden auch Punkte darüber und darunter berücksichtigt*

* Um einen bestimmten Ausschnitt zu machen, muss der Parameter wahr sein >> übersetzen und in Position drehen.

* Wird benötigt, um Objekte im DXF- oder SVG-Format zu exportieren.

Projektion:

    projection(cut=true) translate([-40, 0, 15]) sphere(20);
    projection(cut=false) sphere(20);
    projection(cut=true) translate([40, 0, 18]) sphere(20);


Ohne Projektion:

<img src="images/2_9_FirstSteps_1.png" width="512"/><br>

Mit Projektion:

<img src="images/2_9_FirstSteps_2.png" width="512"/><br>

---

#### [Zurück zum Index](#index)

### 2.10. Export <a name="export"></a>

* OpenSCAD bietet verschiedene Exportformate.
* Zum Exportieren muss die **F6**-Taste gedrückt werden, um das Objekt zuerst zu rendern.
* 3D-Formate sind STL (empfohlen), OFF, AMF, 3MF, CSG.
* 2D-Formate sind DXF, SVG, PNG-Bild.
* Um Objekte im 2D-Format (DXF oder SVG) zu exportieren, muss project() verwendet werden.

---

#### [Zurück zum Index](#index)

### 2.11. Import <a name="import"></a>

* Der Import erfolgt über den Befehl import().
* OpenSCAD unterstützt derzeit die 3D-Formate STL, OFF, AMF und 3MF sowie die 2D-Formate DXF, SVG.
* Wenn Sie eine andere SCAD-Datei importieren möchten, müssen Sie stattdessen include<filename.scad> verwenden.
* Wenn Sie ein Bild importieren möchten, können Sie surface() verwenden.
* Parameter von import() sind:

  **file**

  *String >> Dies ist der Pfad zur Datei einschließlich des Dateinamens.*

  *Es ist nicht absolut, sondern relativ zum importierenden Skript.*

  *Wenn include<> mit einem Skript verwendet wird, das import() verwendet, ist es relativ zu dem Skript, das include<> verwendet.*

  *Wenn Sie ein Absolut verwenden möchten.*

  **convexity**

  *Ganze Zahl >> Wird nur zur Optimierung der Visualisierung im Vorschaumodus verwendet*

  **layer**

  *Ganze Zahl >> Nur DXF, es wird nur ein bestimmter Layer importiert*

  **surface()**

  *liest Heightmap-Informationen aus Text oder einem Bild. Es kann PNG-Dateien lesen*

* Parameter von surface() sind:

  **file**

  *Text >> Der Pfad zu der Datei, die die Heightmap-Daten enthält*

  **center**

  *Boolean (Default: false) >> Dies bestimmt die Positionierung des generierten Objekts*

  *Wenn wahr, wird das Objekt in der X- und Y-Achse zentriert*

  *Bei false wird das Objekt im positiven Quadranten platziert*

  **invert**

  *Boolean (Standard: false) >> Kehrt um, wie der Farbwert von Bildern in Höhe übersetzt wird*

  *Es hat keine Auswirkungen auf importierte Textdateien*

  **convexity**

  *Integer >> Konvexität Parameter gibt die maximale Anzahl von Vorderseiten (Rückseiten) an*

  *ein Strahl, der das Objekt schneidet, könnte durchdringen*

  *Dies wird nur für eine bessere Vorschau verwendet, wenn Sie F5 drücken, hat es keinen Einfluss auf F6*

#### Übung:

***Erstelle deine eigene Visitenkarte oder andere Textgrafiken in 2D. Wenn du ein Bild für den Text verwenden möchten, verwende Import. Wenn du fertig bist, exportiere es im dxf-Format!***

Importieren - Importieren Sie Ihre Möbel! Zuerst als stl exportieren, dann über import() wieder importieren:

    import("files/potFace.stl");

<img src="images/2_11_FirstSteps_1.png" width="512"/><br>

Importieren - Importieren Sie ein SVG-Format und extrudieren Sie es in eine 3D-Form:

    linear_extrude(height=5, center=true, convexity=10)
    translate([-107, -130, 0])   //It gets the graphic in offset of the coordinate system
    import("files/bunnySkull.svg");

<img src="images/2_11_FirstSteps_2.png" width="512"/><br>

***Hinweis: Sie können Inkscape verwenden, eine weitere Open-Source-Software, um Ihre eigenen Grafiken im SVG-Format zu erstellen***

Oberfläche - Importieren Sie Heightmap aus einer Heightmap-Textdatei:

    scale(5)
    rotate([0, 0, -90])
    surface(file="files/surface.dat", center=true);

<img src="images/2_11_FirstSteps_3.png" width="512"/><br>

Oberfläche - Heightmap über ein Bild im PNG-Format importieren:

    scale([0.2, 0.2, 0.012])
    translate([0, 0, 12])
    surface(file="files/escooter_1920.png", center=true);

<img src="images/2_11_FirstSteps_4.png" width="512"/><br>


---

#### [Zurück zum Index](#index)

### 2.12. Schleifen <a name="loops"></a>

Schleifen sind Iteratorfunktionen, die als Operator in OpenSCAD verwendet werden (keine Semikolons).

Eine for()-Schleife wertet jeden Wert in einem Bereich von Vektoren aus und wendet ihn auf die folgende Aktion an.

Es erstellt einen Baum von Objekten, einen Zweig für jedes Mal.

Die Standardsyntax von for() ist:

  >for(variable=[start:incement:end]) {action() object();} or action;<br>
  for(variable=[start:end]) {action() object();} or action;<br>
  for(variable=[vector]) {action() object();} or action;

* Parameter von for() sind:

  **start**

  *Ursprünglicher Wert*

  **increment**

  *Betrag, um den Wert zu erhöhen (optional, Standard=1)*

  **end**

  *Stoppen, wenn der nächste Wert nach dem Ende wäre*


* Eine Intersektion_for()-Schleife iteriert über die Werte in einem Vektorbereich und erzeugt die Schnittmenge von Objekten, die bei jedem Durchlauf erzeugt werden.
* Abgesehen davon, dass für jeden Durchgang separate Instanzen erstellt werden, gruppiert der Standard for() auch alle diese Instanzen.
* Erstellen einer impliziten Union, während section_for() die implizite Union umgeht, um ein Ergebnis zu erhalten.
* Indem Sie eine Kombination der standardmäßigen for()- und Kreuzung()-Anweisungen verwenden
* interface_for()-Schleife verwendet dieselben Parameter wie die for()-Schleife

Schleifen: Iteration über einen Vektor von Vektoren (Rotation):

    for(i=[[0, 0, 0], [10, 20, 300], [200, 40, 57], [20, 88, 57]])
    {
        rotate(i)
        cube([100, 20, 20], center=true);
    }

<img src="images/2_12_FirstSteps_1.png" width="512"/><br>

Schleifen: Iteration über einen Vektor von Vektoren (Translation)

    for(i=[[0, 0, 0], [10, 12, 10], [20, 24, 20], [30, 36, 30], [20, 48, 40], [10, 60, 50]])
    {
        translate(i)
        cube([50, 15, 10], center=true);
    }

<img src="images/2_12_FirstSteps_2.png" width="512"/><br>

Schnittschleife: Schleife über einen Bereich mit einem Ausschnitt aus überlappenden übersetzten Kugeln als Ergebnis

    intersection_for(n=[1:6])
    {
        rotate([0, 0, n*60])
        translate([20, 0, 0])
        sphere(r=40);
    }

<img src="images/2_12_FirstSteps_3.png" width="512"/><br>

Schnittschleife: Schleife über einen Bereich mit einem Ausschnitt aus überlappenden gedrehten Blöcken als Ergebnis

    intersection_for(i=[[0, 0, 0], [10, 20, 300], [200, 40, 57], [20, 88, 57]])
    {
        rotate(i)
        cube([200, 40, 40], center=true);
    }

<img src="images/2_12_FirstSteps_4.png" width="512"/><br>


---

#### [Zurück zum Index](#index)

### 2.13. Statements <a name="statements"></a>

Sind Tests, die anhand einer Bedingung prüfen, ob eine Aktion in einem Unterbereich ausgeführt werden soll oder nicht.

* Die if()-Anweisung hat die folgende Standard-Syntax:

  >if(test) scope1<br>
  >if(test) {scope1}<br>
  >if(test) scope1 else scope2<br>
  >if(test) {scope1} else {scope2}<br>

* Parameter von if() sind:

  **test**

  *Normalerweise ein boolescher Ausdruck, kann aber ein beliebiger Wert oder eine beliebige Variable sein (nicht verwechseln = mit ==)*

  **scope1**

  *Eine oder mehrere Aktionen, die ausgeführt werden sollen, wenn der Test wahr ist*

  **scope2**

  *Eine oder mehrere Aktionen, die ausgeführt werden sollen, wenn der Test falsch ist*

* Es gibt weitere Aussagen, auf die wir nicht weiter eingehen: Bedingt ? und lass(). Dies ist jedoch für den Kurs nicht notwendig!

* Bedingt ? ist eine Funktion, die einen Test verwendet, um zu bestimmen, welcher von 2 Werten zurückgegeben werden soll

* Die Syntax lautet:
  >a = test ? TrueValue : FalseValue ;

* Die Parameter von ? sind:

  **test**

  *Normalerweise ein boolescher Ausdruck, kann aber auch etwas anderes sein*

  **TrueValue**

  *Der zurückzugebende Wert, wenn test wahr ist*

  **FalseValue**

  *Der zurückzugebende Wert, wenn test false ist >> Die Werte können Zahlen, Strings, ein Vektor sein*

  *oder der undefinierte Wert (undef)*

Anweisungen: if()-Anweisungen, die zum Ausführen bestimmter Aktionen verwendet werden, die einer Zahl zugeordnet sind

    a=0;
    b=0;   
    c=1;
    x=0;
    y=0;
    z=0;
    if (b==c) color("Red") cube([40, 10, 30]);
    if (b<a) {color("Green") cube([20, 20, 10], center=true);
    color("Yellow") cylinder(60, 10);}
    if (a>b) translate([0, 50, 0]) color("SeaGreen") text("a is greater than b");
    else if(a<b) color("Khaki") translate([0, 60, 0]) text("b is greater than a");
    else color("Peru") translate([0, 40, 0]) text("b is equal to a");
    if (a==4) echo("a is 4");
    if (x&&y) {color("White") cube([10, 40, 5]); color("Black") cylinder(h=7, d=30);}
    if (x!=y) {color("Orange") cube([20, 40, 5],center=true); color("Blue") cylinder(h=20, d=10, center=true);};
    if (z) {color("SkyBlue") cylinder(40, 20, 10);} else {echo("z is 0");}
    if ((b<5)&&(c>8)) {color("HotPink") cylinder(h=70, d=10, $fn=40);}

|    |    |
|----|----|
| <img src="images/2_13_FirstSteps_1.png" width="256"/>  |  <img src="images/2_13_FirstSteps_2.png" width="256"/> |
| <img src="images/2_13_FirstSteps_3.png" width="256"/>  | <img src="images/2_13_FirstSteps_4.png" width="256"/>  |


***Gehe jede Zeile durch und überprüfe das Ergebnis, während du die Werte von a,b,x,y,z änderst. Ausführen mit F5!***

#### Nicht für den Kurs

Hier einige Beispiele für bedingte ?

    a=1; b=2; c= a==b ? "a==b" : "a!=b" ;
    TrueValue = true; FalseValue = false;
    a=5; test = a==1;
    echo( test ? TrueValue : FalseValue );
    L = 75; R = 2; test = (L/R)>25;
    TrueValue = [test,L,R,L/R,cos(30)];
    FalseValue = [test,L,R,sin(15)];
    a1 = test ? TrueValue : FalseValue ;
    "a!=b"
    false
    [true, 75, 2, 37.5, 0.866025]

---

#### [Zurück zum Index](#index)

### 2.14. Module <a name="modules"></a>

* Kann verwendet werden, um Objekte zu definieren oder mit children() Operatoren zu definieren

* Einmal definiert, werden Module temporär zur Sprache hinzugefügt, ähnlich wie Bibliotheken in anderen Sprachen

* Standardverwendung:

  >modul_name(parameter) {aktionen}

  >modul_name - Ihr Name für das Modul. Versuchen Sie, etwas Sinnvolles auszuwählen
  >parameter  - Null oder mehr Argumente. Sie können als Standardwerte und/oder Variablen zur Verwendung in Aufrufen zugewiesen werden. Parameter sind lokal, sodass sie nicht mit externen Variablen mit demselben Namen in Konflikt geraten
  >aktionen     - Fast jede außerhalb eines Moduls gültige Anweisung kann in ein Modul eingeschlossen werden, einschließlich Funktionsdefinitionen und anderer Module.

* Solche Funktionen und Module können nur innerhalb des umgebenden Moduls selbst aufgerufen werden.

* Die Modulvariablen können einem Modul extern zugewiesen werden, jedoch nicht umgekehrt.

* Module geben keine Werte nach außen zurück

* Objektmodule >> OpenSCAD-Objektmodule wie 3D-Primitive sind eingebettete Module, also gute Beispiele. Objektmodule müssen mit einem Semikolon ";" enden.

* Operatormodule >> Die Verwendung von children() ermöglicht es Modulen, als Operatoren zu fungieren, die auf beliebige oder alle Objekte innerhalb dieser Modulinstanz angewendet werden.

* Operatormodule müssen nicht mit einem Semikolo enden.

* Kinder sind indizierte Objekte über ganze Zahlen von 0 bis Kinder-1

OpenSCAD setzt untergeordnete Elemente auf die Gesamtzahl der Objekte innerhalb des Geltungsbereichs. Gruppierte Objekte werden als 1. behandelt

Syntaxverwendung von children():

  >children();                         //all children<br>
  >children(index);                    //value or variable to select one child<br>
  >children([start:step:end]);         //select from start to end incremented by step<br>
  >children([start:end]);              //step defaults to 1 or -1<br>
  >children([vector]);                 //selection of several children


Module: Implementieren von Parametern, die parametrisch geändert werden können, sodass der Modulaufruf die Objekte entsprechend ändert:

    ColorBreak=[[0, ""],
                [20, "lime"], // upper limit of color range
                [40, "greenyellow"],
                [60, "yellow"],
                [75, "LightCoral"],
                [200, "red"]];
    Expense=[16, 20, 25, 85, 52, 63, 45];
    //First we build a color bar, that will be nested into another module
    module ColorBar(value,period,range)
    { // 1 color on 1 bar
        RangeHi = ColorBreak[range][0];
        RangeLo = ColorBreak[range-1][0];
        color( ColorBreak[range][1] )
        translate([10*period,0,RangeLo])
        if (value > RangeHi) cube([5,2,RangeHi-RangeLo]);
        else if (value > RangeLo) cube([5,2,value-RangeLo]);
    }

Nun das Modul, welches anschließend aufgerufen wird. Beachte, dass dies das ColorBar-Modul als Parameter verwendet:

    module ShowColorBars(values)
    {
        for (month = [0:len(values)-1], range =
        [1:len(ColorBreak)-1])
        ColorBar(values[month],month,range);
    }

Rufe das Modul auf:

    translate([-30, -20, 0])
    ShowColorBars(Expense);


<img src="images/2_14_FirstSteps_1.png" width="512"/><br>

***Ändere nun die Werte in Expense=[] und drücke erneut F5!***

Beispiel für ein manipuliertes Modul. Es ist nicht möglich, dieses Objekt parametrisch zu ändern:

    module arrow(){
        color("Black") cylinder(100, 5, 5);
        color("Red") cube([40, 5, 30], true);
        color("Red") cube([5, 40, 30], true);
        translate([0, 0, 100]) color("Red") cylinder(40, 20, 0, true);
    }

Rufe das einfache Modul auf

    arrow();

<img src="images/2_14_FirstSteps_2.png" width="512"/><br>


Erstelle ein Kanonen Modul:

    size=5;
    angle=30;
    module cannon(size=10, angle=60){
        scale(size)
        union()
        {
            rotate([0, 0, 90])
            translate([0, 0, 10])
            difference()
            {
                cube([40, 30, 20], true);
                translate([0, 0, 5]) cube([50, 20, 15], true);
            }
            translate([0, -15, 20])
            rotate([angle-90, 0, 0])
            difference()
            {
                union()
                {
                    sphere(10);
                    cylinder(40, 10, 8);
                }
                cylinder(41, 4, 4);
            }
        }
    }

Nennen Sie das Modul mit Größe und Winkel als Variable

    cannon(size,angle);

<img src="images/2_14_FirstSteps_3.png" width="512"/><br>


Modul >> Objekte entlang der x-Achse multiplizieren:

    module linear_array(count,distance)
    {
        for(i=[0:1:count-1])
        {
            translate([distance*i, 0, 0])
            children();
        }
    }

Rufen Sie das Pfeil-Modul auf:

    linear_array(4, 300)
    cannon(size, angle);

<img src="images/2_14_FirstSteps_4.png" width="512"/><br>

***Schwierige Aufgabe: Ändern Sie das linear_array in ein x,y,z-Modul, indem Sie die if()-Schleife verwenden!***

Verwenden Sie ganze Zahlen, um eine Achse zuzuweisen:

    axis=0; //x: 0, y: 1, z: 2
    module linear_array(axis,count,distance)
    {
        //If value is 0, the axis is x
        if(axis==0)
        {
            //Loop it according to count value
            for(i=[0:1:count-1])
            {
                //Translate children along x
                translate([distance*i, 0, 0])
                children();
            }
        }
        //If value is 1, the axis is y
        if(axis==1)
        {
            //Loop it according to count value
            for(i=[0:1:count-1])
            {
                translate([0, distance*i, 0])
                children();
            }
        }
        //If value is 2, the axis is z
        if(axis==2)
        {
            //Loop it according to count value
            for(i=[0:1:count-1])
            {
                translate([0, 0, distance*i])
                children();
            }
        }
    }

Rufen Sie das Pfeil-Modul auf:

    linear_array(0, 8, 300)
    cannon(size, angle);

<img src="images/2_14_FirstSteps_4.png" width="512"/><br>


---

#### [Zurück zum Index](#index)

### 2.15. Variablen <a name="variables"></a>

Variablen sind wie Platzhalter für Werte, die Zahlen, Zeichenfolgen, boolesche Werte usw. enthalten können.

Eine Variable hat einen Namen, der frei gewählt werden kann, aber aussagekräftig sein sollte, um spätere Verwechslungen zu vermeiden.

Es ersetzt "hartcodierte" Werte, die nicht parametrisch/austauschbar sind.

Wenn Sie 100-Objekte mit einem bestimmten "hartcodierten" Wert verwenden, müssen Sie 100 Werte ändern, während bei Variablen nur ein Wert geändert werden muss.

Die Verwendung von "hartcodierten" Werten wird als statisch bezeichnet und ist in einigen Fällen gut zu verwenden oder sogar notwendig.

Andererseits ist die Verwendung von Variablen parametrisch, was Flexibilität des Designs bietet.

Syntaxverwendung:
  >variable_name = value; i.e. object(variable_name);

Jede Variablenzuweisung muss mit einem Semikolon ";" enden.

#### ÜBUNG:

*1. Ändere die festen Zahlen deiner früheren Designs in Variablen.*<br>
*2. Erstelle ein Modul aus einem der Objekte, indem du die Variablennamen verwendest.*<br>
*3. Rufe das Modul auf und teste, ob sich die Änderung von Werten auf dein Design auswirkt.*<br>
*4. Erstelle ein weiteres Modul, das Translation und Rotation in einem Aufruf durchführt. Verwende tx,ty,tz,rx,ry,rz als Variablennamen.*<br>
*5. Teste, ob sich dein Objekt bewegt und dreht, indem du die Werte deines Betriebsmoduls änderst.*<br>
---

#### [Zurück zum Index](#index)

## 3. Beispiele <a name="examples"></a>

In diesem Abschnitt findest du Beispiele, die dir helfen werden, die im Abschnitt 2 erworbenen Kenntnisse zu vertiefen.

---

#### [Zurück zum Index](#index)

### 3.1. Beispiele: 3D Formen <a name="e3dshapes"></a>

**3D-Formen: Dodekaeder durch Schnittpunkt von 6 Boxen**

    module dodecahedron(height)
    {
        scale([height, height, height]) //Scale by height parameter
        {
            intersection()
            {
                cube([2, 2, 1], center=true); //Make a cube
                intersection_for(i=[0:4])  //Loop i from 0 to 4, and intersect results
                {
                    rotate([0, 0, 72*i])     //Rotate object below 72*i degrees around Z axis
                    rotate([116.565, 0, 0])  //Rotate object below 116.565 degree around X axis
                    cube([2, 2, 1], center=true);
                }
            }
        }
    }

Erstelle nun ein Dodekaeder:

    dodecahedron(30);

<img src="images/3_1_FirstSteps_1.png" width="512"/><br>


**3D-Formen: Ikosaeder durch Schnittpunkt von 3 Rechtecken mit goldenem Schnitt**

    module icosahedron(size)
    {
        phi=0.5*(sqrt(5)+1);    //The Golden Ratio
        st=size/10000;          //Microscopic sheet thickness
        hull()                  
        {
            cube([size*phi, size,st], true);  //First rectangle
            rotate([90, 90, 0]) cube([size*phi, size, st], true);    //Second rectangle
            rotate([90, 0, 90]) cube([size*phi, size, st], true);    //Third rectangle
        }
    }

Erstelle nun ein Ikosaeder aus dem Modul:

    size=10;
    icosahedron(size);

<img src="images/3_1_FirstSteps_2.png" width="512"/><br>


**3D-Formen: Erstelle eine Wellenhalterung**

    fn = 60;                    // Kreisauflösung
    eps = 0.03;                 // Kleine Nummer
    slop = 0.2;                 // Drucker-Slop: Schrumpfung der ID eines Lochs
    // Grundwerte der Konsolenplattform
    bp_width = 30;              // Breite der Trägerplattform
    bp_length = 40;             // Länge der Halterungsplattform
    bp_thickness = 2;           // Dicke der Halterungsplattform
    screw_size = 3.0 + slop;    // Durchmesser des Befestigungsschraubenlochs
    // Grundwerte der Welle
    shaft_diameter = 8.0+slop;  // Wellendurchmesser
    shaft_thickness = 1.5;      // Dicke des Wellenbundes
    //Grundwerte der Bügelschelle
    bc_area = 2;                // Dicke des Bügelklemmbereichs
    bc_length = 20;             // Länge des Bügelklemmbereichs
    bc_width = screw_size*2.8;          // Spannblockbreite
    bc_height = bc_length;             //Spannblockhöhe
    bc_thickness = 3;           // Spannblockdicke
    // Grundwerte der Bügelschelle
    notch_width = 1;            //Breite der Kerbe in der Halterungsklemme
    notch_length = 15;          //Länge der Halterungskerbe
    bf_height = 5;              //Konsole Winkelflanschhöhe
    //Andere Variablen
    bcol = shaft_diameter+2*shaft_thickness; //Bügelkragen OD
    shaft_length = bc_length + eps*2;        //Schaftlänge für Ausschnitt
    //Modul, das Klammern erstellt
    module bracket()
    {
        difference()
        {
            union()
            {
                translate([-bp_width/2,-bp_length/2,0]) cube([bp_width,bp_length,bp_thickness]); //Flat platform-top
                translate([0,0,-bc_length]) cylinder(r = bcol/2, h=bc_length, $fn=fn); //Outer round clamp
                rotate([180,0,0]) cylinder(r1=(bp_width+bcol)/4, r2=bcol/2, h=bf_height, $fn=fn); //Flange
                translate([-bc_thickness/2,shaft_diameter/2,-bc_length]) cube([bc_thickness,bc_width,bc_height]); //Shaft clamp block
            }
            translate([bp_width/2 - 1.5*screw_size,bp_length/2 - 1.5*shaft_diameter,-(eps)])
            cylinder(r=screw_size/2,h=bp_thickness+2*eps,$fn=12);  //Mounting hole
            translate([bp_width/2 - 1.5*screw_size,-bp_length/2 + 1.5*shaft_diameter,-(eps)])
            cylinder(r=screw_size/2,h=bp_thickness+2*eps,$fn=12);  //Mounting hole
            translate([-bp_width/2 + 1.5*screw_size,bp_length/2 - 1.5*shaft_diameter,-(eps)])
            cylinder(r=screw_size/2,h=bp_thickness+2*eps,$fn=12);  //Mounting hole
            translate([-bp_width/2 + 1.5*screw_size,-bp_length/2 + 1.5*shaft_diameter,-(eps)])
            cylinder(r=screw_size/2,h=bp_thickness+2*eps,$fn=12);  //Mounting hole
            //translate([-notch_width/2,-(bcol+eps)/2,-(bc_area+eps)])  //Notch both sides
            translate([-notch_width/2,0,-(bc_length+eps)])       //Notch one side only
            cube([notch_width,bcol+bc_height+2*eps,notch_length]);       //Notch cutout in cylinder
            translate([0,0,eps-shaft_length]) cylinder(r = shaft_diameter/2, h=shaft_length, $fn=fn);  //Hole for shaft
            translate([-bc_width/2,bcol/2 + screw_size,-bc_length+screw_size]) rotate([0,90,0])
            cylinder(r=screw_size/2,h=bc_width+2*eps,$fn=12);  //Hole in shaft clamp block
            //translate([0,0,-25]) color("red") cube([50,50,50]);  //DEBUG cutaway
        }
    }

Anruf mit gedrehter Klammer:

    rotate([180,0,0]) bracket();


<img src="images/3_1_FirstSteps_3.png" width="512"/><br>


**3D-Formen: Erstelle eine einfache Schüssel**

    //Basisvariablen mit Werten initiieren
    resolution=20;
    diameter=80;
    depth=40;
    middle_wall=true; //für Müsli, das nicht matschig wird
    flat_bottom=5;
    wall_thickness=2;
    middle_resolution=3;
    middle_wave=10;
    $fn = resolution;

Modul für Schüssel erstellen

    module bowl(){
        hull()
        {
            translate([0,0,diameter/2-flat_bottom]) sphere(d=diameter);
            translate([0,0,depth]) sphere(d=diameter);
        }
    }
    //Negativform um Schüssel hohl zu machen
    module taken_bowl()
    {
        intersection()
        {
            scale([(diameter/(diameter+(wall_thickness*2))),(diameter/(diameter+(wall_thickness*2))),1]) bowl();
            translate([0,0,wall_thickness]) cylinder(d=diameter, h=depth);
        }
    }
    //Make the bowl hollow
    intersection()
    {
        difference()
        {
            bowl();
            taken_bowl();
        }
        cylinder(d=diameter, h=depth);
    }
    //If true, add a separation wall
    if(middle_wall == true){
        intersection(){
            taken_bowl();
            middle_sine(depth);
        }
    }
    //Separation wall in bowl
    module middle_sine(depth)
    {
        r = 0;
        h = 0;
        w = depth;
        // amplitude
        s = 3;
        step = middle_resolution;
        // cycles
        c= middle_wave;
        // small number
        e=0.02;
        module sinwave(w)
        {
            for(a=[0:step:diameter-step])
            {
                hull()
                {
                translate([r-w,a,-2*s])
                cube([2*w,e,2*s+s*sin(c*a)]);
                translate([r-w,a+step,-2*s])
                cube([2*w,e,2*s+s*sin(c*(a+step))]);
                }
            }
        }
        translate([0,-diameter/2,0])
        rotate([0,90,0])
        difference()
        {
            sinwave(w);
            translate([0,0,-wall_thickness]) sinwave(w=w*2);
        }
    }


<img src="images/3_1_FirstSteps_4.png" width="512"/><br>

**3D-Form: Erstelle eine einfache Tasse**

    // Basisvariablen mit zugewiesenen Werten
    diameter=50;
    height=60;
    wall_thickness=2;
    handle_thickness=8;
    front_icon="";
    front_icon_size=30;
    icon_depth=5;
    bottom_icon="";
    icon_size=25;
    resolution=10;
    bottom_thickness=5;
    hand_size=20;
    handle=true;
    handle_height=-2;
    handle_resolution=5;
    $fn= resolution;
    // Modul für positive Becherform
    module mug_added(diameter, height, hand_size, height, handle_thickness, handle_height, handle_resolution){
        cylinder(d=diameter, h=height);
        if(handle == true){
            handle(hand_size, height, handle_thickness, handle_height, handle_resolution);
        }
    }
    // Modul für Negativausschnitt der Tasse innen
    module mug_taken(diameter, height, wall_thickness, bottom_thickness){
        translate([0,0,bottom_thickness]) cylinder(d=diameter-(wall_thickness*2), h=height);
    }
    // Modul für Bechergriff
    module handle(hand_size, height, handle_thickness, handle_height, handle_resolution){
        $fn = handle_resolution;
        translate([hand_size,0,(height-hand_size-(handle_thickness/2)+handle_height)])
        rotate([90,0,0])
        rotate_extrude()
        {
            translate([hand_size,0,0]) circle(d=handle_thickness);
        }
    }
    // Modul zum Zusammenbauen einer Tasse ohne Text
    module mug_no_text(diameter, height, hand_size, height, handle_thickness, handle_height, handle_resolution)
    {
        difference()
        {
            mug_added(diameter, height, hand_size, height, handle_thickness, handle_height, handle_resolution);
            mug_taken(diameter, height, wall_thickness, bottom_thickness);
        }
    }

Tasse erstellen:

    difference(){
        mug_no_text(diameter, height, hand_size, height, handle_thickness, handle_height, handle_resolution);
        scale([-1,1,1])
        translate([0,-diameter/2+icon_depth/2,height/2])
        rotate([90,0,0])
        linear_extrude(height = bottom_thickness/2)
        {
            text(text = bottom_icon, font = "fontawesome", size = icon_size, halign = "center", valign = "center");
        }
    }
    intersection(){
        difference(){
            translate([0,0,height/2])
            rotate([90,0,0])
            linear_extrude(height = diameter) {
                text(text = front_icon, font = "fontawesome", size = front_icon_size, halign = "center", valign = "center");
            }
            mug_taken(diameter, height, wall_thickness, bottom_thickness);
        }
        mug_added(diameter=diameter+icon_depth, height, hand_size, height, handle_thickness, handle_height, handle_resolution);
    }

<img src="images/3_1_FirstSteps_5.png" width="512"/><br>


---

#### [Zurück zum Index](#index)

### 3.2. Beispiele: 2D Formen <a name="e2dshapes"></a>

**2D-Formen: Erstelle eine extrudierte Ngon-Ringform**

    height=3; //Height of z extrusion
    outerSize=20; //Outer diameter
    innerSize=18; //Inner diameter
    corners=6;
    module ngonRing(innerSize,outerSize,height,corners)
    {
        linear_extrude(height)
        difference()
        {
            circle(outerSize,$fn=corners);
            circle(innerSize,$fn=corners);
        }
    }
    ngonRing(innerSize,outerSize,height,corners);

<img src="images/3_2_FirstSteps_1.png" width="512"/><br>


**2D-Formen: Erstelle eine komplexe runde Form**

    function circle(radius) = [for (phi = [0 : 1 : 720]) [radius * cos(phi/2), radius * sin(phi)]];
    color("red") polygon(circle(20));

<img src="images/3_2_FirstSteps_2.png" width="512"/><br>


**2D-Formen: Erstelle ein abgerundetes Quadrat**

    size=[20,20]; //Dedicated by x and y position
    radius=5; //Dedicated smoothness of rounded edge
    module roundedSquare(size,r=radius) {
        minkowski() {
            square([size[0]-radius*2,size[1]-radius*2],center=true);
            circle(r=radius);
        }
    }
    roundedSquare(size,r=radius);

<img src="images/3_2_FirstSteps_3.png" width="512"/><br>

**2D-Formen: Erstelle ein Ngon, eine 2D-Form mit einer Variablen von n Seiten und einem benutzerdefinierten Radius**

    sides = 8;
    radius = 20;
    // The orientation might change with the implementation of circle...
    module ngon(sides, radius, center=false){
        rotate([0, 0, 360/sides/2]) circle(r=radius, $fn=sides);
    }
    ngon(sides, radius, center=false);

<img src="images/3_2_FirstSteps_4.png" width="512"/><br>

**2D-Formen: Erstelle eine Ellipse, die geviertelt werden kann**

    width=20; //Size in x
    height=30; //Size in y
    numQuarters=1; //1-4 für jedes Quartal
    module ellipse(width, height) {
        scale([1, height/width, 1]) circle(r=width/2);
    }
    module ellipsePart(width,height,numQuarters)
    {
        o = 1; //slight overlap to fix a bug
        difference()
        {
            ellipse(width,height);
            if(numQuarters <= 3)
                translate([0-width/2-o,0-height/2-o,0]) square([width/2+o,height/2+o]);
            if(numQuarters <= 2)
                translate([0-width/2-o,-o,0]) square([width/2+o,height/2+o*2]);
            if(numQuarters < 2)
                translate([-o,0,0]) square([width/2+o*2,height/2+o]);
        }
      }
      ellipsePart(width,height,numQuarters);

<img src="images/3_2_FirstSteps_5.png" width="512"/><br>


**2D-Formen: Erstelle eine Schildkröten-Spiralform**

    // Länge bestimmt den Abstand
    length = 100;
    // line_width bestimmt die Breite der Linie
    line_width = 1.2;
    // line module draws a 2D line from p1 to p2
    module line(p1,p2,w) {
        hull() {
            translate(p1) circle(r=w,$fn=20);
            translate(p2) circle(r=w,$fn=20);
        }
    }
    // Die Turtle-Funktion ergibt einen 2D-Punkt (x,y) die Position der Turtle und einen Winkel, der die Ausrichtung der Turtle bestimmt
    function turtle(x, y, angle) = [[x, y], angle];
    // Die Vorwärtsfunktion führt dazu, dass die Schildkröte über eine Distanz bewegt wird, bei der die neue x- und y-Position der Schildkröte berechnet wird, dass die Schildkröte (Schildkröte[1]) gleich bleibt
    function forward(turtle, length) =
        turtle(
            turtle[0][0] + length * cos(turtle[1]),
            turtle[0][1] + length * sin(turtle[1]),
            turtle[1]
        );
    // Die Wendefunktion führt zu einer neuen Ausrichtung der Schildkröte, bei der der Winkel zur alten Ausrichtung hinzugefügt wird, um die neue Ausrichtung zu bestimmen. Die Position der Schildkröte bleibt gleich.
    function turn(turtle, angle) = [turtle[0], turtle[1] + angle];
    // Das rekursive Modul move_the_turtle bewegt sich entlang eines Pfades, der durch Länge, Winkel und Linien bestimmt wird. Länge und Winkel können sich im rekursiven Modul Linien ändern bestimmt die Anzahl der Linien, die von der Schildkröte gezeichnet werden.
    module move_the_turtle(turtle, length, angle, lines, line_width){
        if ( lines > 0 ) {
            new_turtle_forwarded = forward(turtle,length);
            line(turtle[0],new_turtle_forwarded[0],line_width);
            new_turtle_turned = turn(new_turtle_forwarded,angle);
            move_the_turtle(new_turtle_turned, length-2, angle, lines-1, line_width);  
        }
    }

Erstelle eine Turtle t mit den obigen Modulen mit Start im Ursprung mit Winkel 0 Grad:

    t1 = turtle(0, 0, 0);
    linear_extrude(height=2.5)
    for (i=[0:7]) {
        for (j=[0:7]) {
            translate([length*i,length*j,0]) move_the_turtle(t1,length,90,50,line_width);
        }
    }

<img src="images/3_2_FirstSteps_6.png" width="512"/><br>

Erstelle ein Dreieck mit den obigen Modulen:

    t2 = forward(t1, length);
    line(t1[0],t2[0],line_width);
    t3 = forward(turn(t2,90), length);
    line(t2[0],t3[0],line_width);
    t4 = forward(turn(t3,135), sqrt(2)*length);
    line(t3[0],t4[0],line_width);

<img src="images/3_2_FirstSteps_7.png" width="512"/><br>

Erstelle einen Turtle-Start mit den obigen Modulen:

    move_the_turtle(t1, 200, 170, 36, 1);

<img src="images/3_2_FirstSteps_8.png" width="512"/><br>


**Polyline: Demonstration von symmetrischen Poly(kubik)beziern.**

    // Die vier schwarzen Punkte sind die Kontrollpunkte des roten Beziers. Die vollständige Kontrolle über die Form der Figur. Die vier Beziers sind durch ihre Endpunkte miteinander verbunden.
    $fn=50;
    // Schnittpunkte auf der x- und y-Achse bestimmen die Lage der Endpunkte auf diesen Achsen. Ändern Sie diese Werte, um die Kurve zu modellieren.
    x_intersection = -14;
    y_intersection = -24;
    // Die Punkte von Bezier1 werden auf die gerade Linie gebracht, um sicherzustellen, dass alle Kurven tangential sind.
    bezier1=[[0,y_intersection],[-8,y_intersection],[x_intersection,-12],[x_intersection,0]];
    // w bestimmt die Breite der Linie (Dicke der Fläche)
    w = 0.5; //[0.5:3]
    // deltat bestimmt die Schrittweite der 'Laufvariablen' t.
    // Je kleiner der Schritt, desto glatter die Kurve
    deltat = 0.05;
    // Kubische Bezier-Funktion
    function cubic_bezier(points) = [for (t=[0:deltat:1+deltat]) pow(1-t,3)*points[0]+3*pow((1-t),2)*t*points[1]+3*(1-t)*pow(t,2)*points[2]+pow(t,3)*points[3]];
    // Modul der Polylinie
    module line(p1,p2,w) {
        hull() {
            translate(p1) circle(r=w,$fn=20);
            translate(p2) circle(r=w,$fn=20);
        }
    }
    // Unter Verwendung des leicht modifizierten Polylinienmoduls von JustinSDK (danke Justin Lin). Siehe seine Dokumentation hier: https://openhome.cc/eGossip/OpenSCAD/Polyline.html. Es ist gut zu lesen.
    module polyline(points, index, w) {
        if(index < len(points)) {
            line(points[index - 1], points[index],w);
            polyline(points, index + 1, w);
        }
    }
    // Dieses Modul macht die Hilfslinien und die Kontrollpunkte sichtbar
    module control_points(bezier1) {
        color("red") polyline(bezier1,1,w/1.5);
        for (i=[0:len(bezier1)-1] ){
            color("black") translate(bezier1[i]) circle(r=1);
        }
    }
    // Erstelle vier Béziers, indem du einfach die Spiegelfunktion von OpenSCAD verwendest. Alle Béziers werden eine andere Farbe haben, um sie zu trennen.
    control_points(bezier1);
    // Verwende den Rumpf, um eine geschlossene Form zu erhalten
    //hull(){
    union()
    {
        color("red") polyline(cubic_bezier(bezier1),1,w);
        color("blue") mirror([0,1,0]) polyline(cubic_bezier(bezier1),1,w);
        color("green") mirror([1,0,0]) polyline(cubic_bezier(bezier1),1,w);
        color("magenta") mirror([0,1,0]) mirror([1,0,0]) polyline(cubic_bezier(bezier1),1,w);
    }
    //}

<img src="images/3_2_FirstSteps_9.png" width="512"/><br>

**Polyline: Erstelle einen Batman-Ausstecher**

    // Erhöhe die Standardoberflächenauflösung
    $fn=50;
    // Erstellt Listen von Punkten, die als Linien- und Kontrollpunkte für Bezierkurven verwendet werden
    poly1 = [[0,22],[2,22],[5,29],[7,14]];
    bezier1 = [poly1[3],[15,7],[23,12],[20,24]];
    bezier2 = [bezier1[3],[50,16],[50,-10],[26,-26]];
    bezier3 = [bezier2[3],[23,-14],[17,-11],[12,-22]];
    bezier4 = [bezier3[3],[9,16],[5,-18],[0,-35]];
    // w bestimmt die Breite der Linie (Dicke der Fläche)
    w = 0.5; //[0.5:3]
    // deltat bestimmt die Schrittweite der 'Laufvariablen' t. Je kleiner der Schritt, desto glatter die Kurve
    deltat = 0.05;    
    function cubic_bezier(points) = [for (t=[0:deltat:1+deltat]) pow(1-t,3)*points[0]+3*pow((1-t),2)*t*points[1]+3*(1-t)*pow(t,2)*points[2]+pow(t,3)*points[3]];
    // Modul, das eine Linie erstellt aus
    module line(p1,p2,w) {
        hull() {
            translate(p1) circle(r=w,$fn=20);
            translate(p2) circle(r=w,$fn=20);
        }
    }
    // Dieses Modul macht die Hilfslinien und die Kontrollpunkte sichtbar
    module control_points(bezier1) {
        color("red") polyline(bezier1,1,w/1.5);
        for (i=[0:len(bezier1)-1] ){
            color("black") translate(bezier1[i]) circle(r=1);
        }
    }
    /* Unter Verwendung des leicht modifizierten Polyline-Moduls von JustinSDK (danke Justin Lin). Siehe seine Dokumentation hier: https://openhome.cc/eGossip/OpenSCAD/Polyline.html. Es ist gut zu lesen. */
    module polyline(points, index, w) {
        if(index < len(points)) {
            line(points[index - 1], points[index],w);
            polyline(points, index + 1, w);
        }
    }
    // Modul, das eine Hälfte der Form erstellt
    module half_bat() {
        polyline(poly1,1,w);
        color("red") polyline(cubic_bezier(bezier1),1,w);
        color("blue") polyline(cubic_bezier(bezier2),1,w);
        color("magenta") polyline(cubic_bezier(bezier3),1,w);
        color("green") polyline(cubic_bezier(bezier4),1,w);
    }
    // Modul, das die Hälfte in eine vollständige Form spiegelt
    module bat_logo() {
        half_bat();
        mirror([1,0,0]) half_bat();
    }
    // Entkommentieren, um alle Kontrollpunkte der Bezierkurve zu visualisieren
    //control_points(bezier1);
    //control_points(bezier2);
    //control_points(bezier3);
    //control_points(bezier4);
    // Erstelle den Ausstecher aus 2 Formen
    union()
    {
        // Erstelle ein Fledermaus-Logo mit einer extrudierten Dicke von 1 mm und einer Versatzbreite von 2 Zoll
        translate([0,0,0])
        linear_extrude(1)
        offset(2)
        bat_logo();
        // Extrudiere ein weiteres Fledermaus-Logo mit 1 mm Breite und 10 mm Höhe/Dicke
        linear_extrude(10)
        bat_logo();
    }

<img src="images/3_2_FirstSteps_10.png" width="512"/><br>

---

#### [Zurück zum Index](#index)

### 3.3. Beispiele: Text <a name="etext"></a>

**Text: Erstelle eine Visitenkarte**

    // Dies ist die Größe einer normalen Karte, aber die Seiten und die Höhe werden gepolstert, wenn sie eingestellt sind
    card_width = 88.9;
    card_height = 50.8;
    card_thickness = .5;
    padding_sides = 5;
    padding_height = 5;
    // Schriftart und Schriftstärke
    text_thickness = 1;
    font = "Liberation Sans:style=Bold";
    // Text für den Namen
    name = "name name";
    name_text_size = 6;
    name_taken = "true";
    // Text für Kontaktdaten
    twitter = "@twitter";
    website = "www.portfolio.net";
    email = "email@gmail.com";
    phone = "(123) 123-1234";
    contact_font_size= 5;
    contact_info_taken = "true";
    // Reihenfolge der Informationen
    order_twitter = 3;
    order_google = 2;
    order_email = 1;
    order_phone = 0;
    spacing = 2;
    // Text für Firmennamen
    company_name = "DigitalStages";
    company_name_font_size = 8;
    company_name_taken = "true";
    // Modul für Kartenbasis
    module base(){
        translate([-padding_sides/2,-padding_height/2,0]) cube([card_width+padding_sides,card_height+padding_height,card_thickness]);
    }
    // Modul für Firmenname
    module company(){
        translate([0,card_height-company_name_font_size,text_thickness/2]) {
        linear_extrude(text_thickness) text(company_name, font=font , size=company_name_font_size);
        }
    }
    // Modul für Name
    module name(){
        translate([card_width-name_text_size,card_height,text_thickness/2]) {
        linear_extrude(text_thickness) rotate([0,0,-90]) text(name, font=font , size=name_text_size);
        }
    }
    // Modul für Twitter-Details
    module twitter(){
        translate([0,order_twitter*contact_font_size+(spacing*order_twitter),text_thickness/2]) {
        linear_extrude(text_thickness) text(twitter, font=font , size=contact_font_size);
        }
    }
    // Modul für Website
    module website(){
        translate([0,order_google*contact_font_size+(spacing*order_google),text_thickness/2]) {
            linear_extrude(text_thickness) text(website, font=font , size=contact_font_size);
        }
    }
    // Modul für E-Mail
    module email(){
        translate([0,order_email*contact_font_size+(spacing*order_email),text_thickness/2]) {
            linear_extrude(text_thickness) text(email, font=font , size=contact_font_size);
        }
    }
    // Telefon
    module phone(){
        translate([0,order_phone*contact_font_size+(spacing*order_phone),text_thickness/2]) {
            linear_extrude(text_thickness) text(phone, font=font , size=contact_font_size);
        }
    }
    // Modul für Kontaktblock
    module contact_info(){
        twitter();
        website();
        email();
        phone();
    }
    difference(){
        base();
        translate([0,0,-card_thickness]) union(){
            contact_info();
            company();
            name();
        }
    }
    // Firmenname vergeben
    if (company_name_taken=="false") {
        translate([0,0,-card_thickness]) company();
    }
    // Kontaktdaten vergeben
    if (name_taken=="false") {
        translate([0,0,-card_thickness]) name();
    }
    // Name vergeben
    if (contact_info_taken=="false") {
        translate([0,0,-card_thickness]) contact_info();
    }

<img src="images/3_3_FirstSteps_1.png" width="512"/><br>

**Text: Verwende die Symbolschrift, um schöne Knöpfe für deine Jacke zu erstellen**

*Denke daran, dass du zuerst die Schriftart "DJ Icons" auf deinem System installierst oder das Font Importer-Skript in "/files/workfiles" verwendest musst!*

    font="DJ Icons";
    string="k";
    size=20;
    cutIn=1;
    transX=0;
    transY=0;
    union()
    {
        difference()
        {
            // Create basic button
            union()
            {
                translate([0,0,2])
                cylinder(h=2,d=30,$fn=80);
                cylinder(h=2,d1=28,d2=30,$fn=80);
            }
            // Use text with icon font an cutout extruded text graphic with difference
            translate([transX,transY,-1])
            linear_extrude(cutIn+1)
            text(string,font=font,size=size,halign="center",valign="center");
        }
        // Create stitch hole
        translate([0,0,5])
        rotate([0,90,0])
        difference()
        {
            cylinder(h=4,d=5,center=true,$fn=20);
            cylinder(h=5,d=2.5,center=true,$fn=20);
        }
    }

<img src="images/3_3_FirstSteps_2.png" width="512"/><br>


**Text: Verwende die Symbolschrift, um ein Zahnradelement zu erstellen**

*Denke daran, dass du zuerst die Schriftart "Gear Icons" auf deinem System installiert hast oder das Font Importer-Skript in "/files/workfiles" verwendest!*

    font="Gears Icons";
    string="n";
    size=25;
    height=3;
    linear_extrude(height)
    text(string, font=font, size=size, halign="center", valign="center");

<img src="images/3_3_FirstSteps_3.png" width="512"/><br>

---

#### [Zurück zum Index](#index)

### 3.4. Beispiele: Transformationen <a name="etransform"></a>

**Transformation: Verwende die Hüllenmethode, um einen einfachen Slot zu erstellen.**

    //Einfacher Schlitz mit Rumpf
    difference()
    {
        hull()
        {
            translate([100, 0, 20])
            cube([20, 20, 40], center=true);
            translate([-100, 0, 20])
            cube([20, 20, 40], center=true);
            translate([100, 300, 20])
            cylinder(h=40, r=12, center=true);
            translate([-100, 300, 20])
            cylinder(h=40, r=12, center=true);
        }
        hull()
        {
            translate([80, 250, 20])
            cylinder(h=40, r=12, center=true);
            translate([-80, 250, 20])
            cylinder(h=40,r=12, center=true);
        }
    }

<img src="images/3_4_FirstSteps_1.png" width="512"/><br>


**Transformation: Verwende Minkowski, um ein Gehäuse zu erstellen**

    $fn=50;
    difference()
    {
        minkowski()
        {
            cube([100,200,100], center=true);
            sphere(10);
        }
        //Chop off the top
        translate([0,0,50]) cube([130,230,100], center=true);
        //Hollow inside
        minkowski()
        {
            cube([80,180,80], center=true);
            sphere(10);
        }
        translate([0,0,-10])
        {
            linear_extrude(100)
            {
                minkowski()
                {
                    square([90,190], center=true);
                    circle(10);
                }
            }
        }   
    }

<img src="images/3_4_FirstSteps_2.png" width="512"/><br>

---

#### [Zurück zum Index](#index)

### 3.5. Beispiele: Extrusion <a name="eextrude"></a>


**Extrudieren: Verdrehte Hörner erstellen**

    //Erschaffe Hörner aus einem verdrehten übersetzten Kreis
    module horn(height=100,radius=30,twist=720,$fn=50)
    {
        radius=radius/4;
        translate([-radius, 0])  //Translate only in 2D
        linear_extrude(height=height, twist=twist, scale=0, $fn=$fn)
        translate([radius, 0])   //Translate only in 2D
        circle(r=radius);
    }
    //Erstelle nun 2 Horn, während eines gespiegelt ist, wodurch ein symmetrisches Paar entsteht.
    translate([30, 0])
    mirror([1, 0, 0])
    horn();
    translate([-30, 0])
    horn();

<img src="images/3_5_FirstSteps_1.png" width="512"/><br>

**Extrudieren: Vase durch Rotationsextrusion erstellen**

    // Radius bestimmt die Größe des Vasenbodens
    radius = 30;
    // drei Punkte (p0,p1,p2) werden benötigt, um die quadratische Bezierkurve zu erstellen
    // vier Punkte (p0,p1,p2,p3) werden benötigt, um die kubische Bezierkurve zu erstellen
    p0 = [radius,0];
    // als Übungsinkrement den y-Wert von p1 (z.B. in 3er Schritten)
    p1 = [120,60];
    p2 = [10,90];
    p3 = [20,150];
    // w determines the width of the line (thickness of the surface)
    w = 5; //[0.5:3]
    // deltat bestimmt die Schrittweite der 'Laufvariablen' t. Je kleiner der Schritt, desto glatter die Kurve
    deltat = 0.05;
    // Funktion für Bezierkurven erstellen
    function bezier(p0,p1,p2) = [for (t=[0:deltat:1+deltat]) pow(1-t,2)*p0+2*(1-t)*t*p1+pow(t,2)*p2];    
    function cubic_bezier(p0,p1,p2,p3) = [for (t=[0:deltat:1+deltat]) pow(1-t,3)*p0+3*pow((1-t),2)*t*p1+3*(1-t)*pow(t,2)*p2+pow(t,3)*p3];
    // Modul für Polylinie erstellen
    module line(p1,p2,w) {
        hull() {
            translate(p1) circle(r=w,$fn=20);
            translate(p2) circle(r=w,$fn=20);
        }
    }
    /*unter Verwendung des leicht modifizierten Poyline-Moduls von JustinSDK (danke Justin Lin). Siehe seine Dokumentation hier: https://openhome.cc/eGossip/OpenSCAD/Polyline.html. Es ist gut zu lesen. */
    module polyline(points, index, w) {
        if(index < len(points)) {
            line(points[index - 1], points[index],w);
            polyline(points, index + 1, w);
        }
    }
    // Jetzt die eigentliche Profilform erstellen
    for (i=[1:30:360]) {
        rotate([0,0,i])
        rotate([90,0,0])
        linear_extrude(4)
        translate([w,0,0])
        polyline(cubic_bezier(p0,p1,p2,p3),1,w);
    }
    // Entkommentiere die Zeilen unten, um eine Vase zu erstellen
    translate([0,0,-5]) cylinder(r=radius+w,h=9,$fn=50);
    rotate_extrude($fn=50)
    //polyline(bezier(p0,p1,p2),1,w);
    polyline(cubic_bezier(p0,p1,p2,p3),1,1);

<img src="images/3_5_FirstSteps_2.png" width="512"/><br>

**Extrudieren: Erstelle ein einfaches dreieckiges Prisma**

    module triangle_prism(b,d,h,ht,sc)
    {
        linear_extrude(height=ht,scale=sc)
        polygon(points=[[0,0],[b,0],[d,h]]);
    }
    triangle_prism(50,30,30,1,1);

<img src="images/3_5_FirstSteps_3.png" width="512"/><br>


---

#### [Zurück zum Index](#index)

### 3.6. Beispiele: Module <a name="emodules"></a>

#### 3.6.1. Objektmodule

**Rekursives Modul: Erstelle einen einfachen Baum**

    module simple_tree(size, dna, n)
    {
        if (n > 0) {
            // trunk
            color("brown")
            cylinder(r1=size/10, r2=size/12, h=size,
            $fn=24);
            // branches
            translate([0, 0, size])
            for(bd = dna) {
                angx = bd[0];
                angz = bd[1];
                scal = bd[2];
                rotate([angx, 0, angz])
                simple_tree(scal*size, dna,
                n-1);
            }
        }
        else // leaves
            color("green")
            scale([1, 1, 3])
            translate([0, 0, size/6])
            rotate([90, 0, 0])
            cylinder(r=size/6, h=size/10);
    }
    // dna is a list of branching data bd of the tree:
    //bd[0] - inclination of the branch
    //bd[1] - Z rotation angle of the branch
    //bd[2] - relative scale of the branch
    dna = [ [12, 80, 0.85], [55,0, 0.6], [62, 125, 0.6], [57, -125, 0.6] ];

Modulaufruf:

    simple_tree(50, dna, 5);

<img src="images/3_6_FirstSteps_1.png" width="512"/><br>


**Objektmodul: Erstelle eine kleine Tasse**

    module mug(width, height, bottom_thickness=2, wall_thickness=5)
    {
        r_of_inside=width/2-wall_thickness;
        difference()
        {
            translate([0, 0, height/2])
            intersection()
            {
                cube([width, width, height], center=true);
                scale([1, 1, height/width])
                sphere(width/2 * sqrt(2));
            }
            translate([0,0,bottom_thickness])
            cylinder(r=r_of_inside, h=height+0.1);
        }
    }

Rufe das Bechermodul an:

  mug(30, 60);

<img src="images/3_6_FirstSteps_2.png" width="512"/><br>


**Objektmodul: Erstelle eine Kompressionshülle**

    // Außendurchmesser der Stange, um die Kappen-ID zu erstellen
    rod_od = 20.0;
    // Kappenstärke in mm (je Seite)
    rod_id = 15.6;
    // Höhe der Kappe in mm
    cap_high = 2.0;
    // Öffnungskegel in mm
    cap_tapper = 0.3;
    // Einschnitte in der Lippe zur Kompression
    cuts = 10.0;

    main_module();

    module main_module(){ // Modul erstellen
        difference(){
            union(){// Gewerkschaft gründen
                // Basiskappe erstellen
                translate ([0,0,0])
                rotate ([0,0,0])
                cylinder(cap_high,(rod_od)/2,(rod_od)/2,$fn=60,true);
                // konischen Stecker erstellen
                translate ([0,0,(cap_high*1.5)]) rotate ([0,0,0]) cylinder(cap_high*2,(rod_id)/2,(rod_id-(cap_tapper*2))/2,$fn=60,true);
            } //Gewerkschaft beenden
            // Subtraktion der Differenz starten
            // Öffnung für Kompression schaffen
            translate ([0,0,(cap_high*1.5)+0.1 ]) rotate ([0,0,0]) cylinder(cap_high*2,(rod_id-(cap_tapper*6))/2, (rod_id-(cap_tapper*6))/2, $fn=60, true);
            // Muster für Kompressionslippe erstellen
            for (i=[0:(360/cuts):360]) {
                // theta ist ein Grad, der durch eine for-Schleife von 0 bis 360 (Grad) festgelegt wird
                theta=i;
                // dies setzt den x-Achsenpunkt basierend auf dem COS des Theta
                x=0+((rod_id/2)-((cap_tapper*3)))*cos(theta);
                // dies setzt den y-Achsenpunkt basierend auf der Sinus des Theta
                y=0+((rod_id/2)-((cap_tapper*3)))*sin(theta);
                // Dadurch entsteht der Kreis oder ein anderes Objekt am XY-Punkt
                translate([x,y,((cap_high*1.5) +.1)])        cylinder(cap_high*2,(cap_tapper*6)/2,(cap_tapper*6)/2,$fn=60,true);
            }// Ende für Schleife für Kreiserstellung
        } // Ende Differenz
    }// Endmodul


<img src="images/3_6_FirstSteps_3.png" width="512"/><br>


**Objektmodul: Schlüssellöcher für Schrauben erstellen**

    module keyhole_mount(thickness,screw_size=3,head_size=6)
    {
        union()
        {
            // Schlüssellochausschnitt
            translate([0, 0, -0.1])
            {
                linear_extrude(thickness)
                {
                    hull()
                    {
                        //circle(screw_size/2, $fn=25);
                        //translate([head_size, 0]) circle(screw_size/2, $fn=25);
                    }
                    translate([head_size, 0]) circle(head_size/2, $fn=25);
                }
            }
            // Rast
            translate([0, 0, thickness/2])
            {
                linear_extrude(thickness)
                {
                    hull()
                    {
                        circle(head_size/2+head_size/8, $fn=25);
                        translate([head_size, 0]) circle(head_size/2+head_size/8, $fn=25);
                    }
                    translate([head_size, 0]) circle(head_size/2, $fn=25);
                }
            }
        }
    }
    // Implementieren Sie nun den keyhole_mount in ein Brett
    difference()
    {
        cube([50, 200, 2]);
        translate([15, 20, 0]) keyhole_mount(2);
        translate([15, 180, 0]) keyhole_mount(2);
    }

<img src="images/3_6_FirstSteps_4.png" width="512"/><br>


### 3.6.1. Operationale Module

**Betriebsmodul: Lineares Array in eine Richtung >> x: 0, y: 1, z: 2**

    module linear_array(axis, count, distance)
    {
        if(axis==0)
        {
            for(i=[0:1:count-1])
            {
                translate([distance*i,0,0])
                children();
            }
        }
        if(axis==1)
        {
            for(i=[0:1:count-1])
            {
                translate([0,distance*i,0])
                children();
            }
        }
        if(axis==2)
        {
            for(i=[0:1:count-1])
            {
                translate([0,0,distance*i])
                children();
            }
        }
    }
    // Verschiebe einige Kugeln in die z-Achse
    linear_array(2, 5, 40) sphere(20);

<img src="images/3_6_FirstSteps_5.png" width="512"/><br>

**Betriebsmodul: Lineares Array in allen 3 Dimensionen, verwenden Sie 1 auf einer Achse, um es rechteckig zu machen**

    module cubic_array(x, y, z, distance_x, distance_y, distance_z)
    {
        for(i=[0:1:x-1])
        {
            for(j=[0:1:y-1])
            {
                for(k=[0:1:z-1])
                {
                    translate([distance_x*i, distance_y*j, distance_z*k])
                    children();
                }
            }
        }
    }
    // Implementiere einen Würfel aus Würfeln
    cubic_array(10, 10, 10, 20, 20, 20) cube(17);

<img src="images/3_6_FirstSteps_6.png" width="512"/><br>


**Betriebsmodul: Rotations-Array, das center=true/false verwendet, um die Neigung jedes Objekts um den Kreis zu ändern**

    //Verwende die Achse, um die Umlaufachse einzustellen >> x: 0, y: 1, z: 2 - wobei die z-Achse einen Fehler zu lösen hat
    module polar_array(radius, count, axis)
    {
        for(i=[0:360/count:360])
        {
            if(axis==0)
            {
                rotate([i, 0, 0])
                translate([0, radius, 0])
                children();
            }
            if(axis==1)
            {
                rotate([0, i, 0])
                translate([radius, 0, 0])
                children();
            }
            if(axis==2)
            {
                rotate([90, 0, i])
                translate([ 0, 0, radius])
                children();
            }
        }
    }
    // Drehe es!
    polar_array(80, 20, 0) cylinder(h=20, r=10, center=true, $fn=30);

<img src="images/3_6_FirstSteps_7.png" width="512"/><br>


---

#### [Zurück zum Index](#index)

### 3.7. Beispiele: Schleifen <a name="eloops"></a>

**Loops: Wellenmodell 1 >> Erstellen von wellenförmigen Designs mit Würfeln mithilfe einer Funktion.**

    cube_size=2;
    h_div=20;
    step=0.1;
    to=15;
    // Funktion, die x- und y-Schritte einnimmt und Schritt für z-Austritt erhält
    function f(p,q) = (p*p + q*q)/h_div;
    // Durchlaufe jeden Würfel und positioniere ihn gemäß den resultierenden x,y,z-Werten
    scale(4)
    for(a=[-1:step:1], b=[-1:step:1])
    {
        x=a*to;
        y=b*to;
        translate([x,y,f(x,y)])
        cube(cube_size, center=true);
    }

<img src="images/3_7_FirstSteps_1.png" width="512"/><br>

**Loops: Wellenmodell 2 >> Erstellen von wellenförmigen Designs mit Würfeln mithilfe einer Funktion**

    h_div=30;
    step=0.1;
    to=15;
    // Funktion, die x- und y-Schritte einnimmt und Schritt für z-Austritt erhält
    function f(p,q) = (p*p + q*q)/h_div;
    // Durchlaufe jeden Würfel und positioniere ihn gemäß den resultierenden x,y,z-Werten
    scale(4)
    for(a=[-1:step:1], b=[-1:step:1])
    {
        // Erstelle x- und y-Werte über den for()-Schleifenindex
        x=a*to;
        y=b*to;
        //x plus step
        xps=(a+step)*to;
        //y plus step
        yps=(b+step)*to;
        // Erstelle ein unteres Dreieck aus den Würfeln über eine Hüllentransformation
        hull()
        {
            // Erstelle nun einen Würfel und verschiebe ihn an die resultierende Position
            //(x,y)
            translate([x,y,f(x,y)])
            cube(size=step, center=true);
            //(x,y+step)
            translate([x,yps,f(x,yps)])
            cube(size=step, center=true);
            //(x+step,y)
            translate([xps,y,f(xps,y)])
            cube(size=step, center=true);
        }
        // Erstelle das obere Dreieck aus den Würfeln über die Hüllentransformation
        hull()
        {
            //Erstelle nun einen Würfel und verschiebe ihn an die resultierende Position
            //(x,y)
            translate([xps,yps,f(xps,yps)])
            cube(size=step, center=true);
            //(x,y+step)
            translate([x,yps,f(x,yps)])
            cube(size=step, center=true);
            //(x+step,y)
            translate([xps,y,f(xps,y)])
            cube(size=step, center=true);
        }
    }


<img src="images/3_7_FirstSteps_2.png" width="512"/><br>


**Loops: Erstelle einen Schlitz mit Bohrlöchern**

    width=20;
    length=100;
    height=2;
    hole_size=5;
    hole_dist=18;
    module holeySlot(width, height, length, hole_size, hole_dist)
    {
        difference()
        {
            hull()
            {
                for(i=[-length/2,length/2])
                    translate([i,0,0]) cylinder(d=width,h=height);
            }
            for(j=[-length/2+width/4:hole_dist:length/2-width/4])
                translate([j,0,0]) cylinder(d=hole_size, h=20);    
        }
    }
    holeySlot(width, height, length, hole_size, hole_dist);

<img src="images/3_7_FirstSteps_3.png" width="512"/><br>

---

#### [Zurück zum Index](#index)

### 3.8. Beispiele: Mathe <a name="emath"></a>

**Mathematik: Erstelle mathematische Knoten**

    // Zufälliger Punkt
    p0 = [-4,0,50];
    p1 = [2,7,4];
    p2 = [6,12,8];
    p3 = [8,24,26];
    p4 = [10,12,-12];
    // Erstelle eine Vektorliste aus zufälligen Punkten
    points = [p0,p1,p2,p3,p4];
    // Radius des Knotens
    w = 18;
    // Nun, es ist pi (oder zumindest eine Annäherung, was soll ich sagen).
    pi = 3.1418;
    // Konstante um Grad in Bogenmaß umzuwandeln
    // Siehe Funktionen sinr(x) and cosr(x)
    m = 180/pi;
    // Linienmodul zeichnet eine 3D-Linie von p1 nach p2
    module line(p1,p2,w) {
        hull() {
            translate(p1) sphere(r=w,$fn=20);
            translate(p2) sphere(r=w,$fn=20);
        }
    }
    // Polyline ist ein rekursives Modul zum Zeichnen von Linien zwischen allen Punkten in der Punktliste.
    module poly3Dline(points, index, w) {
        if(index < len(points)) {
            //color([(index/90)%1,(index/45)%1,(index/30)%1]) //change color
            line(points[index - 1], points[index],w);
            poly3Dline(points, index + 1,w);
        }
    }
    // Konvertieren Sie die Sinusfunktion von Grad in Bogenmaß.
    function sinr(x) = sin(m * x);
    // Kosinusfunktion von Grad in Bogenmaß umrechnen.
    function cosr(x) = cos(m * x);
    // Von Paul Bourke erstellter Knoten (Knoten 4), bei dem Beta zwischen 0 und pi. läuft.
    // Bitte beachten Sie, dass die Formeln von Paul Bourke im Bogenmaß angegeben sind.
    function paulBourkeKnot(beta) = let(
        r = 100 * (0.8 + 1.6 * sinr(6 * beta)),
        theta = 2 * beta,
        phi = 0.6 * pi * sinr(12 * beta),
        x = r * cosr(phi) * cosr(theta),
        y = r * cosr(phi) * sinr(theta),
        z = r * sinr(phi)
    ) [x,y,z];
    // Der einfachste nicht-triviale Knoten der Trefoil-Knoten
    function trefoilKnot(t) = let (
        x = sin(t) + 2 * sin(2*t),
        y = cos(t) - 2 * cos(2*t),
        z = -sin(3*t)
    ) [x,y,z];
    // Der Achterknoten
    function figureEightKnot(t) = let (
        x = (2 + cos(2*t)) * cos(3*t),
        y = (2 + cos(2*t)) * sin(3*t),
        z = -sin(4*t)
    ) [x,y,z];
    // Uncomment-Funktion unten, um die Punkte für den trefoilKnot zu erhalten.
    function collectKnotPoints() = [for (i = [0:2:360]) 30 *trefoilKnot(i)];
    // Uncomment-Funktion unten, um die Punkte für die FigurEightKnot zu erhalten.
    //function collectKnotPoints() = [for (i = [0:2:360]) 30 * figureEightKnot(i)];
    // Entkommentieren Sie diese Funktion, um einen Punkt für den Paul-Bourke-Knoten (Knoten 4) zu generieren.
    //function collectKnotPoints() = [for (i = [0:pi/400:pi+pi/400]) paulBourkeKnot(i)];
    poly3Dline(collectKnotPoints(),1,w);


<img src="images/3_8_FirstSteps_1.png" width="512"/><br>


**Mathematik: Erstelle mehrere Formen (unten erklärt) mit mathematischen Grundfunktionen von OpenSCAD**

    // Eine EPITROCHOID ist eine Kurve, die von einem Punkt verfolgt wird, der in einem Abstand "d" zum Mittelpunkt eines Kreises mit Radius "r" festgelegt ist, wenn der Kreis außerhalb eines anderen Kreises mit Radius "R" rollt.
    // Eine HYPOTROCHOIDE ist eine Kurve, die von einem Punkt verfolgt wird, der in einem Abstand "d" zum Mittelpunkt eines Kreises mit Radius "r" festgelegt ist, während der Kreis in einem anderen Kreis mit Radius "R" rollt.
    // Ein EPICYCLOID ist ein Epitrochoide mit d = r.
    // Ein HYPOCYCLOID ist ein Hypotrochoide mit d = r.
    // Siehe http://en.wikipedia.org/wiki/Epitrochoid und http://en.wikipedia.org/wiki/Hypotrochoid
    // Vorsicht vor den Polarformen der Gleichungen auf Wikipedia...
    // Sie sind richtig, aber Theta wird bis zur Mitte der kleinen Scheibe gemessen!!
    // Es gibt verschiedene Verfahren zum Extrudieren. Die besten sind wahrscheinlich diejenigen, die lineares Extrudieren verwenden.
    //===========================================
    // Demo - zeichnet jeweils eines, plus einige kleine Räder und Stöcke.
    // Lustige Sachen zum Ausprobieren:
    // Animieren, versuchen Sie es mit FPS = 5 und Schritten = 200
    // R = 2, r = 1, d = 0.2
    // R = 4, r = 1, d = 1
    // R = 2, r = 1, d = 0.5
    // Was passiert, wenn Sie d > r machen??
    // Was passiert, wenn d < 0 ??
    // Was passiert, wenn r < 0 ??
    //===========================================

    // Lege grundlegende Variablen für alle Beispiele fest - Entkommentiere diese für jedes Beispiel.
    $fn = 30;
    thickness = 10;
    R = 4;
    r = 1;
    d = 1;
    n = 60; // Anzahl Keilsegmente
    alpha = 360*$t;
    p = 16;
    rb = 30;
    twist = 250;

Modul für Epitrochoide erstellen:

    module epitrochoid(R, r, d, n, thickness)
    {
        dth = 360/n;
        for ( i = [0:n-1] )
        {
            polyhedron(points = [[0,0,0],
            [(R+r)*cos(dth*i) - d*cos((R+r)/r*dth*i), (R+r)*sin(dth*i) - d*sin((R+r)/r*dth*i), 0],
            [(R+r)*cos(dth*(i+1)) - d*cos((R+r)/r*dth*(i+1)), (R+r)*sin(dth*(i+1)) - d*sin((R+r)/r*dth*(i+1)), 0],
            [0,0,thickness],
            [(R+r)*cos(dth*i) - d*cos((R+r)/r*dth*i), (R+r)*sin(dth*i) - d*sin((R+r)/r*dth*i), thickness],
            [(R+r)*cos(dth*(i+1)) - d*cos((R+r)/r*dth*(i+1)), (R+r)*sin(dth*(i+1)) - d*sin((R+r)/r*dth*(i+1)), thickness]],
            faces = [[0, 2, 1],
            [0, 1,  3],
            [3, 1, 4],
            [3, 4, 5],
            [0, 3, 2],
            [2, 3, 5],
            [1, 2, 4],
            [2, 5, 4]]);
        }
    }
    epitrochoid(R, r, d, n, thickness);

<img src="images/3_8_FirstSteps_2.png" width="512"/><br>


Modul für Hypotrochoide erstellen:

    module hypotrochoid(R, r, d, n, thickness)
    {
        dth = 360/n;
        for ( i = [0:n-1] )
            {
                polyhedron(points = [[0,0,0],
                [(R-r)*cos(dth*i) + d*cos((R-r)/r*dth*i), (R-r)*sin(dth*i) - d*sin((R-r)/r*dth*i), 0],
                [(R-r)*cos(dth*(i+1)) + d*cos((R-r)/r*dth*(i+1)), (R-r)*sin(dth*(i+1)) - d*sin((R-r)/r*dth*(i+1)), 0],
                [0,0,thickness],
                [(R-r)*cos(dth*i) + d*cos((R-r)/r*dth*i), (R-r)*sin(dth*i) - d*sin((R-r)/r*dth*i), thickness],
                [(R-r)*cos(dth*(i+1)) + d*cos((R-r)/r*dth*(i+1)), (R-r)*sin(dth*(i+1)) - d*sin((R-r)/r*dth*(i+1)), thickness]],
                faces = [[0, 2, 1],
                        [0, 1,  3],
                        [3, 1, 4],
                        [3, 4, 5],
                        [0, 3, 2],
                        [2, 3, 5],
                        [1, 2, 4],
                        [2, 5, 4]]);
            }
    }
    hypotrochoid(R, r, d, n, thickness);

<img src="images/3_8_FirstSteps_3.png" width="512"/><br>


Modul für einen Epitrochoidenkeil mit Bohrung erstellen:

    module epitrochoidWBore(R, r, d, n, p, thickness, rb)
    {
        dth = 360/n;
        union() {
            for ( i = [0:p-1] )
            {
                polyhedron(points = [[rb*cos(dth*i), rb*sin(dth*i),0],
                [(R+r)*cos(dth*i) - d*cos((R+r)/r*dth*i), (R+r)*sin(dth*i) - d*sin((R+r)/r*dth*i), 0],
                [(R+r)*cos(dth*(i+1)) - d*cos((R+r)/r*dth*(i+1)), (R+r)*sin(dth*(i+1)) - d*sin((R+r)/r*dth*(i+1)), 0],
                [rb*cos(dth*(i+1)), rb*sin(dth*(i+1)), 0],
                [rb*cos(dth*i), rb*sin(dth*i), thickness],
                [(R+r)*cos(dth*i) - d*cos((R+r)/r*dth*i), (R+r)*sin(dth*i) - d*sin((R+r)/r*dth*i), thickness],
                [(R+r)*cos(dth*(i+1)) - d*cos((R+r)/r*dth*(i+1)), (R+r)*sin(dth*(i+1)) - d*sin((R+r)/r*dth*(i+1)), thickness],
                [rb*cos(dth*(i+1)), rb*sin(dth*(i+1)), thickness]],
                faces = [[0, 1, 4], [4, 1, 5],
                        [1, 2, 5], [5, 2, 6],
                        [2, 3, 7], [7, 6, 2],
                        [3, 0, 4], [4, 7, 3],
                        [4, 5, 7], [7, 5, 6],
                        [0, 3, 1], [1, 3, 2]]);
            }
        }
    }
    epitrochoidWBore(R, r, d, n, p, thickness, rb);

<img src="images/3_8_FirstSteps_4.png" width="512"/><br>


Modul für einen Epitrochoidenkeil mit Bohrung erstellen, Linear extrudieren:


    module epitrochoidWBoreLinear(R, r, d, n, p, thickness, rb, twist)
    {
        dth = 360/n;
        linear_extrude(height = thickness, convexity = 10, twist = twist)
        {
            union()
            {
                for ( i = [0:p-1] )
                {
                    polygon(points = [[rb*cos(dth*i), rb*sin(dth*i)],
                    [(R+r)*cos(dth*i) - d*cos((R+r)/r*dth*i), (R+r)*sin(dth*i) - d*sin((R+r)/r*dth*i)],
                    [(R+r)*cos(dth*(i+1)) - d*cos((R+r)/r*dth*(i+1)), (R+r)*sin(dth*(i+1)) - d*sin((R+r)/r*dth*(i+1))],
                    [rb*cos(dth*(i+1)), rb*sin(dth*(i+1))]],
                    paths = [[0, 1, 2, 3]], convexity = 10);
                }
            }
        }
    }
    epitrochoidWBoreLinear(R, r, d, n, p, thickness, rb, twist);

<img src="images/3_8_FirstSteps_5.png" width="512"/><br>


Modul für einen Epitrochoidenkeil, Lineare Extrusion erstellen:

    module epitrochoidLinear(R, r, d, n, p, thickness, twist)
    {
        dth = 360/n;
        linear_extrude(height = thickness, convexity = 10, twist = twist)
        {
            union()
            {
                for ( i = [0:p-1] )
                {
                    polygon(points = [[0, 0],
                    [(R+r)*cos(dth*i) - d*cos((R+r)/r*dth*i), (R+r)*sin(dth*i) - d*sin((R+r)/r*dth*i)],
                    [(R+r)*cos(dth*(i+1)) - d*cos((R+r)/r*dth*(i+1)), (R+r)*sin(dth*(i+1)) - d*sin((R+r)/r*dth*(i+1))]],
                    paths = [[0, 1, 2]], convexity = 10);
                }
            }
        }
    }
    epitrochoidLinear(R, r, d, n, p, thickness, twist);

<img src="images/3_8_FirstSteps_6.png" width="512"/><br>


Modul für einen Hypotrochoidenkeil mit Bohrung erstellen:

    module hypotrochoidWBore(R, r, d, n, p, thickness, rb)
    {
        dth = 360/n;
        union()
        {
            for ( i = [0:p-1] )
            {
                polyhedron(points = [[rb*cos(dth*i), rb*sin(dth*i),0],
                [(R-r)*cos(dth*i) + d*cos((R-r)/r*dth*i), (R-r)*sin(dth*i) - d*sin((R-r)/r*dth*i), 0],
                [(R-r)*cos(dth*(i+1)) + d*cos((R-r)/r*dth*(i+1)), (R-r)*sin(dth*(i+1)) - d*sin((R-r)/r*dth*(i+1)), 0],
                [rb*cos(dth*(i+1)), rb*sin(dth*(i+1)), 0],
                [rb*cos(dth*i), rb*sin(dth*i), thickness],
                [(R-r)*cos(dth*i) + d*cos((R-r)/r*dth*i), (R-r)*sin(dth*i) - d*sin((R-r)/r*dth*i), thickness],
                [(R-r)*cos(dth*(i+1)) + d*cos((R-r)/r*dth*(i+1)), (R-r)*sin(dth*(i+1)) - d*sin((R-r)/r*dth*(i+1)), thickness],
                [rb*cos(dth*(i+1)), rb*sin(dth*(i+1)), thickness]],
                faces = [[0, 1, 4], [4, 1, 5],
                        [1, 2, 5], [5, 2, 6],
                        [2, 3, 7], [7, 6, 2],
                        [3, 0, 4], [4, 7, 3],
                        [4, 5, 7], [7, 5, 6],
                        [0, 3, 1], [1, 3, 2]]);
            }
        }
    }
    hypotrochoidWBore(R, r, d, n, p, thickness, rb);

<img src="images/3_8_FirstSteps_7.png" width="512"/><br>


Erstellen Sie einen Hypotrochoidenkeil mit Bohrung, linearer Extrusion:

    module hypotrochoidWBoreLinear(R, r, d, n, p, thickness, rb, twist)
    {
        dth = 360/n;
        linear_extrude(height = thickness, convexity = 10, twist = twist)
        {
            union()
            {
                for ( i = [0:p-1] ) {
                    polygon(points = [[rb*cos(dth*i), rb*sin(dth*i)],
                    [(R-r)*cos(dth*i) + d*cos((R-r)/r*dth*i), (R-r)*sin(dth*i) - d*sin((R-r)/r*dth*i)],
                    [(R-r)*cos(dth*(i+1)) + d*cos((R-r)/r*dth*(i+1)), (R-r)*sin(dth*(i+1)) - d*sin((R-r)/r*dth*(i+1))],
                    [rb*cos(dth*(i+1)), rb*sin(dth*(i+1))]],
                    paths = [[0, 1, 2, 3]], convexity = 10);
                }
            }
        }
    }
    hypotrochoidWBoreLinear(R, r, d, n, p, thickness, rb, twist);

<img src="images/3_8_FirstSteps_8.png" width="512"/><br>


Erstellen Sie einen Hypotrochoidenkeil:

    module hypotrochoidLinear(R, r, d, n, p, thickness, twist)
    {
        dth = 360/n;
        linear_extrude(height = thickness, convexity = 10, twist = twist)
        {
            union()
            {
                for ( i = [0:p-1] )
                {
                    polygon(points = [[0, 0],
                    [(R-r)*cos(dth*i) + d*cos((R-r)/r*dth*i), (R-r)*sin(dth*i) - d*sin((R-r)/r*dth*i)],
                    [(R-r)*cos(dth*(i+1)) + d*cos((R-r)/r*dth*(i+1)), (R-r)*sin(dth*(i+1)) - d*sin((R-r)/r*dth*(i+1))]],
                    paths = [[0, 1, 2]], convexity = 10);
                }
            }
        }
    }
    hypotrochoidLinear(R, r, d, n, p, thickness, twist);

<img src="images/3_8_FirstSteps_9.png" width="512"/><br>


---

#### [Zurück zum Index](#index)

### 3.9. Beispiele: Variablen <a name="evariables"></a>


**Variablen: Platte / Halterung: Parametrisches Design für Möbel**

    // Modul für Einzelplatte
    module plate(width, length, thickness, screw_size, head_size)
    {
        difference()
        {
            cube([width, length, thickness]);
            translate([width/2, length/2, 0])
            cylinder(h=thickness, r=screw_size, $fn=30);
            translate([width/2, length/2, thickness-4])
            cylinder(h=4, r1=1, r2=head_size, $fn=30);
        }
    }
    // Modul für Halterung
    module bracket(length, width, thickness, screw_size, head_size)
    {
        plate(width, length, thickness, screw_size, head_size);
        translate([width,-thickness,thickness])
        rotate([90,0,180])
        plate(width, length, thickness, screw_size, head_size);
        translate([0, -thickness, 0])
        cube([width, thickness, thickness]);
    }
    // Rufen Sie das Plattenobjekt auf
    bracket(50,50,4,3,6);

<img src="images/3_9_FirstSteps_1.png" width="512"/><br>

---

#### Ende von Teil 1:

1. Ich hoffe, Dir hat der Kurs gefallen.
2. Wenn nach dem Kurs noch Fragen offen sind, senden Sie mir bitte eine E-Mail.
3. Vergesse nicht, sich in die E-Mail-Liste meiner Webseite einzutragen, wenn du am Aufbaukurs teilnehmen möchtest.
4. Der zweite Teil wird dich in die Bibliothek der Dinge eingeführen, eine Datenbank mit Möbeln und Haushaltsgegenständen, welche mit OpenSCAD designed und geschrieben sind. Gemeinsam werden wir ein Möbelstück erzeugen, welches mit einer CNC Maschine.

**Vielen Dank!**

---

Erstellt von [Jens Meisner](https://www.jensmeisner.net)

Bitte kontaktieren Sie mich, wenn Sie Fragen, Empfehlungen oder Feedback haben. Sehr geschätzt!

contact@jensmeisner.net
